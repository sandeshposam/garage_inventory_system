-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2020 at 11:33 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `garage_inventory_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `i_id` int(200) NOT NULL,
  `i_uid` varchar(200) DEFAULT NULL,
  `i_barcodeid` varchar(200) DEFAULT NULL,
  `i_product_name` varchar(200) DEFAULT NULL,
  `i_product_category` varchar(200) DEFAULT NULL,
  `i_product_description` varchar(200) DEFAULT NULL,
  `i_product_rate` varchar(200) DEFAULT NULL,
  `i_product_count` varchar(200) DEFAULT NULL,
  `i_product_manufacturer` varchar(200) DEFAULT NULL,
  `i_product_rack` varchar(200) DEFAULT NULL,
  `i_product_total` varchar(200) DEFAULT NULL,
  `i_created_by` varchar(200) DEFAULT NULL,
  `i_created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`i_id`, `i_uid`, `i_barcodeid`, `i_product_name`, `i_product_category`, `i_product_description`, `i_product_rate`, `i_product_count`, `i_product_manufacturer`, `i_product_rack`, `i_product_total`, `i_created_by`, `i_created_at`) VALUES
(0, 'x6couO15q0enVQsXPM2E', NULL, 'abc', 'abc', 'dvdvdv', '300', '5', NULL, '1', '1500', NULL, '2020-01-16 13:23:05'),
(1, NULL, '12345', 'castrol', 'oil', 'sdgggxnjfx jfg', '5000', '1', 'castrol', '2', NULL, NULL, '2020-01-16 10:49:28'),
(2, NULL, '123456', 'break oil', 'oil', 'sdgsdd', '5000', '2', 'shbxgbxfgx', '3', NULL, NULL, '2020-01-16 10:49:28');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `s_id` int(200) NOT NULL,
  `s_uid` varchar(200) DEFAULT NULL,
  `s_date` varchar(200) DEFAULT NULL,
  `s_invoice` varchar(200) DEFAULT NULL,
  `s_desc` text DEFAULT NULL,
  `s_total` varchar(200) DEFAULT NULL,
  `s_tax` varchar(200) DEFAULT NULL,
  `s_customer_name` varchar(200) DEFAULT NULL,
  `s_phone` varchar(200) DEFAULT NULL,
  `s_discount` varchar(200) DEFAULT NULL,
  `s_payment_mode` varchar(200) DEFAULT NULL,
  `s_credit` varchar(200) DEFAULT NULL,
  `s_created_by` varchar(200) DEFAULT NULL,
  `s_created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`s_id`, `s_uid`, `s_date`, `s_invoice`, `s_desc`, `s_total`, `s_tax`, `s_customer_name`, `s_phone`, `s_discount`, `s_payment_mode`, `s_credit`, `s_created_by`, `s_created_at`) VALUES
(1, 'rTKtp1jJkdiSozcN9A83', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-16 10:33:12'),
(2, 'Kh8X5ywnlpx3MgdNzQZS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-16 10:42:24'),
(3, '5Q6GEatHABD9Z7TRFcSW', '2020-01-15', '65486132', NULL, NULL, NULL, 'nitesh', '7738171808', NULL, 'cash', 'paid', NULL, '2020-01-16 10:42:55'),
(4, 'aIgGUcfHQbqt6jEAz93K', '2020-01-15', '65486132', NULL, NULL, NULL, 'nitesh', '7738171808', NULL, 'cash', 'paid', NULL, '2020-01-16 10:49:43'),
(5, 'TBAqM7sUG1cotzdhESyP', '2020-01-15', '65486132', NULL, NULL, NULL, 'nitesh', '7738171808', NULL, 'cash', 'paid', NULL, '2020-01-16 10:51:16'),
(6, 'kiE6cyJOzY74QpmZFRIM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-16 13:05:56'),
(7, 'GaOeIi3VJDK6qk2szXhp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-16 13:07:13'),
(8, '7jnqrMZJoTCiz50kWpvS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-16 13:23:30'),
(9, 'yulUxf6hAim3PVODKqGv', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-16 13:26:48'),
(10, '7tRDHKdVLeJ3oYGQzmM2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-16 13:28:40'),
(11, '4jqaDZ6hSiMOCFz09cL1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-16 14:34:34'),
(12, 'E2wXsVASvoRtMjH1FC0N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-16 14:35:29'),
(13, 'QO3h62z8wGxcobrHgpa7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-17 07:18:01'),
(14, 'UIWwcOzNu2nv4A7MSg1L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-17 09:28:45'),
(15, 'Ikz6XtYSnxdsP3mNJrD2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-17 09:49:22'),
(16, 'olIyrL7jp28waSBGPRVK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-17 09:49:37'),
(17, '2WrAcbPG5B73qhj4YQa9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-17 10:11:05'),
(18, 'tmJXBMk2Gg0huI8OqpEH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-17 10:11:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`s_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `s_id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
