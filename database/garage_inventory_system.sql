-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2020 at 03:02 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `garage_inventory_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `credit_transactions`
--

CREATE TABLE `credit_transactions` (
  `ct_id` int(11) NOT NULL,
  `ct_uuid` varchar(36) DEFAULT NULL,
  `ct_sales_uid` varchar(36) DEFAULT NULL,
  `ct_amount` varchar(10) DEFAULT NULL,
  `ct_payment_mode` varchar(100) DEFAULT NULL,
  `ct_bank_name` varchar(1000) DEFAULT NULL,
  `ct_cheque_no` varchar(100) DEFAULT NULL,
  `ct_cheque_date` date DEFAULT NULL,
  `ct_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ct_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_transactions`
--

INSERT INTO `credit_transactions` (`ct_id`, `ct_uuid`, `ct_sales_uid`, `ct_amount`, `ct_payment_mode`, `ct_bank_name`, `ct_cheque_no`, `ct_cheque_date`, `ct_created_at`, `ct_status`) VALUES
(1, 'jUnihZSmsqavY1uxDEIC', '9ICMQbHGLzr807dEhSaR', NULL, 'cash', NULL, NULL, NULL, '2020-01-27 14:32:26', 1),
(2, 'neX5aByNG182z9VjsZ6q', '9ICMQbHGLzr807dEhSaR', NULL, 'cash', NULL, NULL, NULL, '2020-01-27 14:32:44', 1),
(3, 'MUqicBpNHCDVPwdJk6SR', '9ICMQbHGLzr807dEhSaR', NULL, 'cash', NULL, NULL, NULL, '2020-01-27 14:33:56', 1),
(4, 'HrXy8MZlY6mqn3waog07', '9ICMQbHGLzr807dEhSaR', '100', 'cash', NULL, NULL, NULL, '2020-01-27 14:37:13', 1),
(5, 'rLCQ6t3MiEqXDpnyTHaP', '9ICMQbHGLzr807dEhSaR', '100', 'cheque', 'SBI', '0002901921', '2020-01-17', '2020-01-27 14:37:32', 1),
(6, 'XCuibxEUajosYAOSHLw6', '9ICMQbHGLzr807dEhSaR', '1800', 'cash', NULL, NULL, NULL, '2020-01-27 14:38:14', 1),
(7, '8Ifa2XehGrQyRJHDxVuo', 'kvGd9xZaA74jowmzWYDT', '600', 'cheque', 'SBI', '321321312', '2020-01-27', '2020-01-27 14:39:37', 1),
(8, 'BdiyA3qfLcN9lprRETta', 'kvGd9xZaA74jowmzWYDT', '200', 'cheque', 'SBI', '786767687687', '2020-01-30', '2020-01-29 14:32:38', 1),
(9, 'eJcj25fPSlQyTnvLU4qi', 'gOr5UJjRKau8CQEPdTGM', '10', 'cash', NULL, NULL, NULL, '2020-01-30 12:29:33', 1),
(10, 'b3Do8qX1kSTHv9riMy2a', 'gOr5UJjRKau8CQEPdTGM', '10', 'cash', NULL, NULL, NULL, '2020-01-30 13:37:03', 1),
(11, 'FGilNrsheJKOmQxIRXnC', 'gOr5UJjRKau8CQEPdTGM', '500', 'cash', NULL, NULL, NULL, '2020-01-30 13:56:21', 1),
(12, 'MR9DzlTeKa01kdjC86w3', 'gOr5UJjRKau8CQEPdTGM', '80', 'cash', NULL, NULL, NULL, '2020-02-05 08:56:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `i_id` int(200) NOT NULL,
  `i_uid` varchar(200) DEFAULT NULL,
  `i_barcodeid` varchar(200) DEFAULT NULL,
  `i_product_quality` varchar(10) DEFAULT NULL,
  `i_product_name` varchar(200) DEFAULT NULL,
  `i_product_category` varchar(200) DEFAULT NULL,
  `i_product_description` varchar(200) DEFAULT NULL,
  `i_product_rate` varchar(200) DEFAULT NULL,
  `i_product_count` varchar(200) DEFAULT NULL,
  `i_product_manufacturer` varchar(200) DEFAULT NULL,
  `i_product_rack` varchar(200) DEFAULT NULL,
  `i_product_compartment` varchar(10) DEFAULT NULL,
  `i_product_barcode` text,
  `i_product_total` varchar(200) DEFAULT NULL,
  `i_created_by` varchar(200) DEFAULT NULL,
  `i_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `i_status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`i_id`, `i_uid`, `i_barcodeid`, `i_product_quality`, `i_product_name`, `i_product_category`, `i_product_description`, `i_product_rate`, `i_product_count`, `i_product_manufacturer`, `i_product_rack`, `i_product_compartment`, `i_product_barcode`, `i_product_total`, `i_created_by`, `i_created_at`, `i_status`) VALUES
(1, 'GoNQFt3MqWZa70O1v4nDSBXiRd5ArsKJwzyC', '401753', '', 'HONDA CITY-t3', 'BREAK CYLINDER', '', '506', '3', 'Calron', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:36', 1),
(2, '145Ntup72KgRfScAXMI3CmQFYqnOVbGPzl8H', '954620', '', 'HONDA CITY-2X/AMAZE', 'BREAK CYLINDER', '', '644', '3', 'Calron', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:36', 1),
(3, 'ptL1EzQjIaMcsvAFyKn6u8J2geHiGDPRmqrl', '682137', '', 'FIAT LINEA/PUNTO', 'BREAK CYLINDER', '', '500', '2', 'Calron', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:36', 1),
(4, 'giTnKOBmS5fHP1AFz38dICk4avZcjEGh0wVs', '728461', '', 'FORD FIEASTA', 'BREAK CYLINDER', '', '500', '2', 'Calron', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:36', 1),
(5, 'qDuOGSbHCzUy4esx5kVr3Z9MLhnPtQ2FdilE', '059327', '', 'CHEVROLET OPTRA', 'BREAK CYLINDER', '', '494', '2', 'Calron', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:36', 1),
(6, 'EdlqiuPSCz63mH8TxXtN57IZLevKwcgkRpbr', '317026', '', 'FORD ICON', 'BREAK CYLINDER', '', '445', '2', 'Calron', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:36', 1),
(7, '6SPNXouBRqZgF4w1iDYb8C0lhfJGtm57M2Hk', '915723', '', 'CHEVROLET BEAT/AVEO', 'BREAK CYLINDER', '', '484', '3', 'Calron', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:36', 1),
(8, 'JDP9zqj7fUgoESGNrkd6YtBOTKZHQsF3hWnI', '482935', '', 'TOYTA QAULIS', 'BREAK CYLINDER', '', '552', '3', 'Calron', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:36', 1),
(9, 'zvUQA1V4tgPBGFcdMSC8qOejYpLlZws0RI5k', '381046', '', 'RENAULT LODGY', 'BREAK CYLINDER', '', '563', '1', 'Calron', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(10, 'WHlFXA76qVhiP0GwUCIkcN3nyQ8u5dO4oKJj', '127950', '', 'CHEVROLT ENJOY RH', 'BREAK CYLINDER', '', '690', '2', 'Calron', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(11, 'zSs4a9LRqH8rXAKC7fbcMPJ02wWltOGFudjQ', '159823', '', 'CHEVROLT ENJOY LH', 'BREAK CYLINDER', '', '690', '3', 'Calron', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(12, 's49qiQlXBhj1KVxmE6kcbtI837y2pTUdLJOR', '513864', '', 'HONDA CITY RH', 'BREAK CYLINDER', '', '450', '1', 'Calron', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(13, '58AdmqVYRUPvIcwB63ngrt9TKCJo7x2GfOp1', '693521', '', 'HONDA CITY LH', 'BREAK CYLINDER', '', '400', '1', 'Calron', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(14, 'd71mE6wMXH9lP3Qe5NhvLorZKJVWFiDcfnIp', '542903', '', 'HEADLIGHT BULB', 'BULB', '60/55 W', '82', '10', 'Osaram', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(15, '7BTAUcRVf8qrn36NGiOtkpEIoF0PH9zvL5um', '491765', '', 'HEADLIGHT BULB', 'BULB', '100/90 W', '110', '10', 'Osaram', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(16, 'JIgEfk4TZme682lSDa7Qx50BNr1sytYMHnOz', '986532', '', 'HEADLIGHT BULB', 'BULB', '100/130 W', '0', '0', 'Osaram', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(17, 'IL6czA4YhX8vVkndFZl2CSxBEK1HqbPeQjgp', '214590', '', 'HEADLIGHT BULB', 'BULB', '60/55 W', '100', '10', 'Philips', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(18, 'P2enq7DgowRtf5Gl8AiExhKNYsa9rBTQbuI1', '938260', '', 'HEADLIGHT BULB', 'BULB', '100/90 W', '205', '10', 'Philips', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(19, 'QspIjTULwuWRk6gP0Ff92SBlXKmZ71VerGJz', '309728', '', 'HEADLIGHT BULB', 'BULB', '100/130 W', '295', '10', 'Philips', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(20, '80ivYHClkXDdzZTf52yLqJgOh9PpmUeVxbs3', '593708', '', 'BULB', 'BULB', '9005/12V/100W', '280', '10', 'Daichi', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(21, 'T0kLY3pOsuhjdDmzHRM4QvFJqwyreU7X2Zbx', '526831', '', 'BULB', 'BULB', 'H16', '380', '5', 'Daichi', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(22, 'Z5KhVvHxp86OSWmn9DyGUodjF3w41tRiCPkf', '361520', '', 'BULB', 'BULB', 'H8 100W', '280', '10', 'Daichi', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(23, 'APa0T85uDSvreoFkpyLmIYXBGMq3zKOdE2xl', '034195', '', 'BULB', 'BULB', '9006 100W', '280', '10', 'Daichi', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(24, 'Rr6Dqbdk8XoGmenLB15V3iKH9xgfA4MCWQSw', '946581', '', 'BULB', 'BULB', 'H11', '280', '10', 'Daichi', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(25, 'NCicuqOm45LV2Wrhvz6QfDRsbA37YFH8UEwX', '395471', '', 'BULB', 'BULB', 'H7', '150', '10', 'Daichi', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(26, 'GKtBuNjI2OcrbdQgRHVEDFhJ9wqfv7CS4Ymo', '176925', '', 'BULB', 'BULB', '881', '230', '10', 'Daichi', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(27, '0lc6Tz1GKrJvsOSPMnNo5y9VB2gYiR8FU4WI', '619723', '', 'BULB', 'BULB', 'H1', '95', '15', 'Daichi', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(28, 'yIafpgMAhRlOY34Dt2uNBZj8ceiKFVXsxWnE', '613527', '', 'BULB', 'BULB', 'H3', '70', '10', 'Daichi', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(29, 'zaXvJNnfQtgT65OKZL407PFWbIrYixs8jcRd', '085741', '', 'BULB', 'BULB', 'Philips 1141', '15', '10', 'Philips', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(30, '6kKhFsDubXqm1SZR04iWcIOAaNEMHQe2f5yt', '943127', '', 'BULB', 'BULB', 'Philips 1016', '20', '10', 'Philips', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(31, 'hKBsv3r8YVnlmgaIGcAEkLe7yF4iZo5uHTwj', '172645', '', 'BULB', 'BULB', 'Osaram 1141', '15', '30', 'Osaram', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(32, '4rnD3yvpzfMhHsBtwZ8xudamT1UA5gcEJl0e', '684913', '', 'BULB', 'BULB', 'Bosch 1', '74', '10', 'Bosch', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(33, '19JF4dzaB6yexbfonA5sOkC3LPYXRmwWlTKi', '352846', '', 'BULB', 'BULB', 'Comet 1141', '15', '10', 'Comet', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(34, 'wugxXRN0DbYF1MLElrmoJ6hzeST84qV3kisQ', '087926', '', 'BULB', 'BULB', 'Imp T20', '60', '10', 'Imported', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(35, 'yF2pi7r1zuOvHQXgcIWj4a6ePSE03YACh5Ko', '384216', '', 'BULB', 'BULB', 'Ideal TD12', '10', '30', 'Ideal', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:37', 1),
(36, 'ELUZwdky7gju5vONnPF8RxQ0S6GI2WCqBiXh', '031465', '', 'BULB', 'BULB', 'Ideal 67', '10', '10', 'Ideal', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(37, 'mRAIlrTHoJFfDCQy0O4PLBjtEwigMs7cdpXY', '237548', '', 'BULB', 'BULB', 'Ideal 1141 Amber/C/P', '15', '10', 'Ideal', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(38, 'zjv12uMD6ZQyT5cI4AYL08FlpkVaJBqKbn7s', '731596', '', 'BULB', 'BULB', 'Ideal 1141 Amber/S/P', '15', '10', 'Ideal', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(39, 'HgiBm3DplrtNhG9z6O7PWXq8UIvbA1cLZ2Fy', '834290', '', 'BULB', 'BULB', 'Philips 9005', '280', '2', 'Philips', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(40, 'ToNrchQtG9g4kBaeuRmLsXUqxflSi13wKA7p', '638471', '', '', 'BULB', 'Wiretape', '10', '30', 'Insulation', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(41, 'R2M9Yq1vjZob0mO7y8A4E63KGgSJ5zkxWfUD', '370865', '', '', 'Consumable', 'Polish paper', '10', '30', 'Consumable', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(42, 'uXMB2yNj5p0ngle1tQov6V7xdmiFO8far4c9', '406518', '', '', 'Consumable', 'Polish paper 80', '10', '30', 'Consumable', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(43, '8NUgF3S6ZTDev1b5V7PhBtGqdim9HsW0n2Ia', '583271', '', '', 'Consumable', 'Grease', '70', '10', 'Consumable', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(44, '1iuQwbfK6pWYoMG4DnBIgJrOE9qT0yNjS2C7', '653198', '', '', 'Consumable', 'Grease 2', '40', '10', 'Consumable', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(45, 'kDNxWeqUMwu9pPT1KacnFm6gCovAyzjXGEhO', '803612', '', '', 'Consumable', 'Grease 3', '20', '10', 'Consumable', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(46, '2Ay5Kx1rGw7WZNmMPv84ac3RVQkqTdLIzBjU', '936027', '', '282N  4AV', '', 'Bosch Rotor', '3460', '1', 'Bosch', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(47, '7Oao2jMu4v6fkHmDbeqypnXYcldNi9C1gIBJ', '689137', '', 'BULB', 'BULB', 'Osaram H7 80W', '160', '10', 'Osaram', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(48, 'y3Vkj8DlId5nWuezBmRxscLUHXaG7NoYb1Qw', '103249', '', 'BULB', 'BULB', 'Osaram H7 55W', '220', '10', 'Osaram', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(49, 'tJIKbxNoZOE3WamnskLcHMpdBTi2RUeGrjzv', '482537', '', 'BULB', 'BULB', 'Osaram H1', '90', '10', 'Osaram', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(50, 'BA1ksn2SJgM06hWwtyr3mdcR7YubfQpI4OTK', '213058', '', 'BULB', 'BULB', 'Philips H7', '320', '10', 'Philips', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(51, 'Ukw2Hiam8O5ue9YG4EZPg0x3jMpsdNQIqhSR', '501867', '', 'BULB', 'BULB', 'Philips H1', '145', '10', 'Philips', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(52, 'sHYZi8mLubDFEXl7gvVxk6Kd5fWOwMITojhC', '076451', '', 'Alto/WagonR (Old)', 'Motor', 'Wipermotor', '1802', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(53, 'c4Ng03keMC9iWtamwFn5JDBlO7LzoQfUpsY6', '627583', '', 'Alto/WagonR (Old)', 'Motor', 'Wipermotor', '1300', '1', 'LMP', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(54, 'cHj7siuKthlXIqFU8mQYbwoLS1dvpZ03xTgA', '102486', '', 'Swift (Old)', 'Motor', 'Wipermotor', '2159', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(55, 'gPe9Vk1HwKFSMECGZL3Xq4IaxsthAinrly8Y', '927603', '', 'Alto/WagonR (Old)', '', 'Stator Altenator', '1674', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(56, '2YRlJIMWT4eycaUowXfSzHDBvqG0V1POKgLi', '593601', '', 'Alto/WagonR (Old)', '', 'Alternator Rotor', '1800', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(57, 'QMEI587GVv6Y31fjdPxTkFoJcARL9tsNHhK2', '280396', '', '5060', '', 'Armature Assembly Starter', '1214', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:38', 1),
(58, 'pI0CqKaFQynJsrb9cWXYL8TVUglmiwDj34Zf', '029375', '', '5210', '', 'Armature Assembly Starter', '1024', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(59, 'm39wqKIoUru5bl7izB1AjxaW6tfE0ce4ORNd', '579418', '', '641A', '', 'Holder Rectifire ', '1789', '2', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(60, 'aO9QPgB26I4wmKATpVZSeMWuUsln8R1F75oz', '438019', '', '5551', '', 'Starter Assy', '3503', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(61, 'Ey4SUpgXju2vNOqBLCYaG5RcQKbs1JwDFM0k', '635942', '', '8510', '', 'Solenoid switch', '1245', '2', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(62, 'GWVip06TBkfsE4x9cnadUzZqA8b1mlDS2hvP', '273591', '', '7800', '', 'Solenoid switch', '1245', '2', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(63, 'dGS0YiBqyKUhImrz3XCtN6E7V41QwTZnPxaf', '597134', '', '6510', '', 'Holder Rectifire ', '1847', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(64, 'JPk5Xsx2AN6pFbhmwZvKG9CqQL4HecfY8USn', '075849', '', '5800', '', 'Starter Bendea', '863', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(65, 'TdgXUkA9SrKYi84Qcatjns3Nu0BbhGCPwLDR', '523986', '', '8460', '', 'Starter Bendea', '920', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(66, 'CJ1xj4z6v0O7LmW85TVbYgNsawqXkfirSGZI', '781235', '', '8880', '', 'Starter Bendea', '849', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(67, 'XfhymkBIv1S8cjblVuT4QKJYLR7rps2CUnwz', '378469', '', '5820', '', 'Starter Bendea', '857', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(68, 'NMWVCHfBjo2zr1ImLhTaZb5g8ecSJdE9lQGt', '083417', '', '1920', '', 'Regulator Assy', '2027', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(69, 'OXr64HAo0IfRNdJGMg8B3tDKUwbv15clhxFz', '264518', '', '3530', '', 'Regulator Assy', '1650', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(70, 'o2pAUBtPsNIMfadrHzg8l1Z4iwbS5L3DqCYv', '195732', '', '2810', '', 'Regulator Assy', '1273', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(71, 'FHpmEyuYAsJQWqf2OeD7rLCnIK36ic9g5NtU', '284697', '', '550', '', 'Regulator Assy', '2400', '2', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(72, 'sluXoLDgyCqaIb4T6tKc8HRMQZ3iGnjdJzpU', '902837', '', '530', '', 'Regulator Assy', '2852', '1', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(73, 'eTtPWXLDA5Ri1VUwckFldq72g40Y93yoEzK8', '457016', '', '2201', '', 'Holder Assy Brush', '263', '7', 'Denso ', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(74, '2V7Si5UryWmYwHPfpktzJeLA4FOKxohEC306', '358429', '', '2870', '', 'Holder Assy Brush', '283', '1', 'Denso ', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(75, 'F4cgAwDReI6TndGqyOZ1C2QxLmY7a5JBKuU0', '389516', '', '3100', '', 'Holder Assy Brush', '584', '2', 'Denso ', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(76, 'wMiKChqU15zSRg4EYXA6fysvN0mrdL9Jux8c', '153297', '', '2801', '', 'Regulator Assy', '1273', '1', 'Denso ', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(77, 'lScozNTvD2M9PuZFE58YpyqOxtdfkHaIX0Kb', '064935', '', '8811', '', 'Pulley', '300', '2', 'Denso', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(78, 'k0Lxc51z9nu3jhiwfE72tM4K6SpNsIFQPJmq', '190532', '', 'Swift 406504', '', 'Starter', '4784', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(79, 'sA4JvIf15EoLQGSq0nZy6dtjKgBzWuX9mPFa', '709152', '', 'Vista/Swift 406592', '', 'Armature', '1812', '2', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(80, 'SAWUcKdzPXot6HwQlyuh8Emq1jZVibC94NRB', '935041', '', 'I10/I20 406551', '', 'Armature', '1968', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(81, 'Whnl8PuqOCJUrGYyfoHLbEmBsTwVgdARKZv5', '176498', '', 'SX4/Ertiga 406580', '', 'Armature', '2054', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(82, 'NTo4W1G37FIdkgiKrSueCbYDUEnBzp5fMacA', '921503', '', 'Polo/Skoda 406594', '', 'Armature', '1550', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(83, '1OyrqxImwzPbD9JtKsG2apoHYNCdRhv3ZiEQ', '706254', '', 'I10/I20 406634', '', 'Carbon Plate', '750', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(84, 'Uxir9NAzWLw1hPatKqGcTZO0SJVvRlQYmp6I', '872031', '', 'Vista/Manza', '', 'Carbon Plate', '750', '2', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:39', 1),
(85, 'VtJ0zAkMsi59CrLXHE8Sl23Tm7nUwg4NIuKp', '349786', '', 'Sx4 406584', '', 'Carbon Plate', '750', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(86, 'EslFi4jN8AHYp06RwzXc71WgP3nKeuyfvtkh', '956842', '', 'Gest/I20 691936', '', 'Carbon Plate', '750', '2', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(87, 'SQIlmCVAhr1izH36eET9j4qLvU7f0tkDBwxN', '953074', '', 'Polo/Vento 40660', '', 'Carbon Plate', '750', '2', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(88, '35vg92zPf7LapYJFeAxCksKNiyTVnmHwrURo', '427380', '', 'I10/I20 406555', '', 'Carbon Plate', '750', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(89, 'fqtpi7Myvd2Lghj6aNln3RPeUk9ZsbQXYSAz', '102385', '', 'I20 406883', '', 'Regulator ', '1950', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(90, '5m8soPLhjrIywCEJtX4gn6TSNkbQMHvDUA17', '863597', '', 'Xcent 691931', '', 'Regulator ', '1530', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(91, 'wPylSUsAHMXuGf0tQVNO3TBq71aZdkRvDo6L', '908571', '', 'Etios 406849', '', 'Regulator ', '1843', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(92, 'LwpEWUoOkC3Z9mYXy7ezxBVcNjI1PMdG6Anr', '785419', '', 'Accent,Elentra,Zest 691615', '', 'Regulator ', '1521', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(93, 'AE9Ye7ghlUxWsSaV5dcQRINkbDfjrvOu2LTo', '412398', '', 'Vista/Linea/Manza 406811', '', 'Regulator ', '1449', '2', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(94, 'tQph8YIqP65kXexfUvs0KNiR1VrBlWZJyG7d', '965843', '', 'Polo/Vento 406599', '', 'Solenoid switch', '1800', '2', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(95, 'yKm26sdbuW8nePcVOCBE1DaiYhf7TFlUk0RI', '156920', '', 'Vista/Manza 406593', '', 'Solenoid switch', '1402', '2', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(96, 'm2AN9osxEwH6UkFj0Y1f5ihDJrLy8V34vbMl', '814635', '', 'Vista/Manza 406593-1', '', 'Solenoid switch', '1368', '2', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(97, 'AZ9oPcf7lDqOJUM3Fsvx5IgCHbB0Rya4VW1h', '790863', '', 'Ertiga 406610', '', 'Solenoid switch', '850', '2', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(98, 'Y4arMvcJBAl1XgQENRuniV90hZztWdbf53yU', '381042', '', 'Swift Dzire 406577', '', 'Solenoid switch', '1390', '2', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(99, 'C7FPS93mYyB4JpqU0IQ2R5jkzTDZMniElthN', '679485', '', 'I10/I20 406554', '', 'Solenoid switch', '1268', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(100, 'RQWBaYuAkCTcHlqbEOi2JUj4hKVGIvo6ZM71', '987016', '', 'Vista/Manza 406810', '', 'Rectifire ', '1550', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(101, 'uplMKnf86J2TdHaQzWGVOADrc5IyZSeRbEF7', '769258', '', 'Swift Dzire 406575', '', 'Starter Bendex', '750', '3', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(102, 'eA61ktQLINZ5o7XvKHyGf2WB8umz0bYRJxEs', '963284', '', 'I10/I20 406552', '', 'Starter Bendex', '850', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(103, '8y6DorGQtm30cuBpR9MVIeznqgACUF7ilakh', '649820', '', 'Indica Cr4 604', '', 'Starter Armature ', '1420', '1', 'Comstar', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(104, 'ZyYO9rbGFPvoiMeDQ2hLtNWH0cEulURqxA35', '523986', '', 'Swift ', '', 'Starter Rod ', '900', '1', 'Valeo', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(105, 'wNg8V5sLerb4hKkW3PU1jlotyQuRdJcIaiXC', '536812', '', 'Swift ', '', 'Starter Rod ', '900', '1', 'Bosch', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(106, 'dCUbSis5EjN39ZG2awnpglcDeJKVPkXuAB8F', '203814', '', 'I20/Verna', '', 'Starter Bendex', '900', '1', 'IMP', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(107, 'glpZCowjP8qDLbUth0YEcAHeR2ivIkx15KrW', '429130', '', 'Vista', '', 'Solenoid switch', '1050', '1', 'Comstar', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(108, 'XOY8xKnArvqlfhdHmg0asT9ibEwcZJRP7UMS', '561738', '', 'WagonR ', '', 'Solenoid switch', '1050', '1', 'Comstar', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(109, 'XsU8z052xo4BVei6DnK9ug7SapCE1PFHjqJb', '392608', '', 'Indica Cr4 603', '', 'Brush holder', '600', '2', 'Comstar', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(110, 'E9x2g0MDcuQyvpLrUGPkRiflJO6YbIBTs4Fa', '320179', '', 'Indica (Old) 106', '', 'Starter Bendex', '780', '1', 'Comstar', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:40', 1),
(111, 'qYf4MeuJWSZanIOmr6tKLT5gRHy37dpPw2Uo', '239760', '', 'Indica Cr4 206', '', 'Starter Bendex', '880', '1', 'Comstar', '', NULL, NULL, NULL, NULL, '2020-02-10 13:50:41', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `login_id` int(11) NOT NULL,
  `login_uuid` varchar(36) DEFAULT NULL,
  `login_email_id` varchar(100) DEFAULT NULL,
  `login_password` varchar(100) DEFAULT NULL,
  `login_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `login_company_name` varchar(1000) DEFAULT NULL,
  `gst_type` varchar(200) DEFAULT NULL,
  `gst_percentage` int(10) DEFAULT NULL,
  `login_status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`login_id`, `login_uuid`, `login_email_id`, `login_password`, `login_created_at`, `login_company_name`, `gst_type`, `gst_percentage`, `login_status`) VALUES
(6, 'g0CohWtFwlfrEAZqx7Bj', 'niteshyeole@zoof.co.in', '123456', '2020-01-30 10:39:13', 'zoof software solutions', NULL, NULL, 1),
(7, 'Bo1K7WTOJXgaE4fknCLz', 'admin@admin.com', '202cb962ac59075b964b07152d234b70', '2020-01-30 10:55:47', 'snp software solutions', NULL, NULL, 1),
(8, 'c0DQ3PtpuIWORUsklHBj', 'sandeshrposam@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '2020-02-07 08:40:17', 'SND Soliution', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE `purchase` (
  `p_id` int(11) NOT NULL,
  `p_uid` varchar(36) DEFAULT NULL,
  `p_vendor_uid` varchar(36) DEFAULT NULL,
  `p_date` date DEFAULT NULL,
  `p_order_no` varchar(100) DEFAULT NULL,
  `p_current_date` date DEFAULT NULL,
  `p_payment_type` varchar(100) DEFAULT NULL,
  `p_created_by` varchar(36) DEFAULT NULL,
  `p_details` text,
  `p_total` varchar(100) DEFAULT NULL,
  `p_remaining_amt` varchar(20) DEFAULT NULL,
  `p_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `p_status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`p_id`, `p_uid`, `p_vendor_uid`, `p_date`, `p_order_no`, `p_current_date`, `p_payment_type`, `p_created_by`, `p_details`, `p_total`, `p_remaining_amt`, `p_created_at`, `p_status`) VALUES
(1, 'VKQCrf8nWUoLBEsS1wHN', '2PrGDanKvtRYLxSepjTi', '2020-01-01', '20200129', '2020-01-29', 'cheque', NULL, '[{\"order\":{\"barcode\":\"1111\",\"qty\":\"100\",\"init_price\":\"1000\"}},{\"order\":{\"barcode\":\"1112\",\"qty\":\"100\",\"init_price\":\"400\"}}]', '140000', NULL, '2020-01-29 09:53:51', 1),
(2, 'hiq0uC3NP74l1sIoDd8g', '2PrGDanKvtRYLxSepjTi', '2020-01-02', '202001291', '2020-01-29', 'credit', NULL, '[{\"order\":{\"barcode\":\"1113\",\"qty\":\"200\",\"init_price\":\"500\"}},{\"order\":{\"barcode\":\"1112\",\"qty\":\"200\",\"init_price\":\"400\"}}]', '180000', '180000', '2020-01-29 09:54:46', 1),
(3, 'LSf3t0H58JZojMuir1lX', '2PrGDanKvtRYLxSepjTi', '0000-00-00', '202001291', '2020-01-29', NULL, NULL, 'null', '0', NULL, '2020-01-29 11:35:21', 1),
(4, '6mYxpz4F8GcQsTeHDw1u', '2PrGDanKvtRYLxSepjTi', '2020-01-16', '202001291', '2020-01-29', 'cash', NULL, '[{\"order\":{\"barcode\":\"1112\",\"qty\":\"1000\",\"init_price\":\"400\"}},{\"order\":{\"barcode\":\"1113\",\"qty\":\"1000\",\"init_price\":\"500\"}},{\"order\":{\"barcode\":\"1111\",\"qty\":\"1000\",\"init_price\":\"1000\"}}]', '1900000', NULL, '2020-01-29 14:33:44', 1),
(5, 'fTNhR0n7VQS2DC8kY5qM', '2PrGDanKvtRYLxSepjTi', '2020-01-30', '202001301', '2020-01-30', 'cash', NULL, '[{\"order\":{\"barcode\":\"1112\",\"qty\":\"100\",\"init_price\":\"400\"}},{\"order\":{\"barcode\":\"1113\",\"qty\":1,\"init_price\":\"500\"}}]', '40500', NULL, '2020-01-30 12:30:46', 1),
(6, 'JeEXnOfHpc2M6tlZqG9C', '2PrGDanKvtRYLxSepjTi', '2020-01-15', '202001301', '2020-01-30', 'cheque', NULL, '[{\"order\":{\"barcode\":\"1112\",\"qty\":\"100\",\"init_price\":\"400\"}}]', '40000', NULL, '2020-01-30 13:37:58', 1),
(7, 'BUxi9L4KZsjqYHwC7lfg', NULL, '0000-00-00', '202001301', '2020-01-30', NULL, NULL, '[{\"order\":{\"barcode\":\"1112\",\"qty\":\"10000\",\"init_price\":\"400\"}}]', '4000000', NULL, '2020-01-30 13:38:25', 1),
(8, '6Iv4nrfbZhHDmiLBa3Tw', NULL, '0000-00-00', '202001301', '2020-01-30', NULL, NULL, '[{\"order\":{\"barcode\":\"1113\",\"qty\":\"1000000\",\"init_price\":\"500\"}}]', '500000000', NULL, '2020-01-30 13:38:42', 1),
(9, 'nOjA4F5sei8Q0JE9maZN', 'z5V8thnKDOjGiUJB9PCA', '2020-01-07', '202001301', '2020-01-30', 'cheque', NULL, '[{\"order\":{\"barcode\":\"1111\",\"qty\":\"1000000\",\"init_price\":\"1000\"}}]', '1000000000', NULL, '2020-01-30 13:39:26', 1),
(10, '6X9EMY8RLrBlqOmGdPaW', 'z5V8thnKDOjGiUJB9PCA', '2020-01-08', '202001301', '2020-01-30', NULL, NULL, '[{\"order\":{\"barcode\":\"1113\",\"qty\":\"100\",\"init_price\":\"500\"}}]', '50000', NULL, '2020-01-30 13:41:00', 1),
(11, 'uI6Q4Vm8wnHv9pBxTRdP', '2PrGDanKvtRYLxSepjTi', '2020-01-30', '202001301', '2020-01-30', 'credit', NULL, '[{\"order\":{\"barcode\":\"1113\",\"qty\":1,\"init_price\":\"5\"}},{\"order\":{\"barcode\":\"1112\",\"qty\":1,\"init_price\":\"400\"}}]', '405', '405', '2020-01-30 13:59:44', 1),
(12, 'RKSs3tocp1MDV7AQzqJb', '2PrGDanKvtRYLxSepjTi', '2020-02-06', '202002051', '2020-02-05', 'cash', NULL, '[{\"order\":{\"barcode\":\"1112\",\"qty\":1,\"init_price\":\"400\"}},{\"order\":{\"barcode\":\"1113\",\"qty\":1,\"init_price\":\"5\"}}]', '405', NULL, '2020-02-05 08:58:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `s_id` int(200) NOT NULL,
  `s_uid` varchar(200) DEFAULT NULL,
  `s_date` varchar(200) DEFAULT NULL,
  `s_invoice` varchar(200) DEFAULT NULL,
  `s_desc` text,
  `s_total` varchar(200) DEFAULT NULL,
  `s_remainig_payment` varchar(10) DEFAULT NULL,
  `s_tax` varchar(200) DEFAULT NULL,
  `s_customer_name` varchar(200) DEFAULT NULL,
  `s_phone` varchar(200) DEFAULT NULL,
  `s_discount` varchar(200) DEFAULT NULL,
  `s_payment_mode` varchar(200) DEFAULT NULL,
  `s_credit` varchar(200) DEFAULT NULL,
  `s_created_by` varchar(200) DEFAULT NULL,
  `s_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`s_id`, `s_uid`, `s_date`, `s_invoice`, `s_desc`, `s_total`, `s_remainig_payment`, `s_tax`, `s_customer_name`, `s_phone`, `s_discount`, `s_payment_mode`, `s_credit`, `s_created_by`, `s_created_at`) VALUES
(1, 'gOr5UJjRKau8CQEPdTGM', '2020-01-30', '20200130', '[{\"order\":{\"barcode\":\"1112\",\"qty\":1,\"init_price\":\"400\"}},{\"order\":{\"barcode\":\"1113\",\"qty\":1,\"init_price\":\"500\"}}]', '900', '300', NULL, 'Sandesh', '9877878787', NULL, 'cheque', 'credit', NULL, '2020-01-30 12:28:42'),
(2, 'Ik1RYXOGazVc0j96QHEL', '2020-01-30', '202001302', '[{\"order\":{\"barcode\":\"1113\",\"qty\":2,\"init_price\":\"500\"}},{\"order\":{\"barcode\":\"1112\",\"qty\":1,\"init_price\":\"400\"}}]', '1400', '0', NULL, 'Nitesh Yeole', '9877878667', NULL, 'cash', 'paid', NULL, '2020-01-30 12:29:07'),
(3, '261OqCXtl9xTUsR50eYk', '2020-01-30', '202001303', '[{\"order\":{\"barcode\":\"1112\",\"qty\":2,\"init_price\":\"400\"}}]', '800', '0', NULL, 'Smitesh More', '88988987689', NULL, 'cheque', 'paid', NULL, '2020-01-30 13:35:15'),
(4, 'ND2buoywHzAGerVp3K9B', '2020-01-30', '202001304', '[{\"order\":{\"barcode\":\"1111\",\"qty\":1,\"init_price\":\"1000\"}},{\"order\":{\"barcode\":\"1112\",\"qty\":2,\"init_price\":\"400\"}}]', '1800', NULL, NULL, 'shreyog', '3534643535', NULL, 'cash', 'paid', NULL, '2020-01-30 13:35:43'),
(5, 'i2XoMRnEdbA95x7WNpZm', '2020-01-30', '202001305', '[{\"order\":{\"barcode\":\"1113\",\"qty\":1,\"init_price\":\"5\"}}]', '5', '5', NULL, 'Smitesh More', '8898876543', NULL, 'cheque', 'credit', NULL, '2020-01-30 13:41:58'),
(6, 'yswbtRiK6oF5ZakYNOUG', '2020-01-30', '202001306', '[{\"order\":{\"barcode\":\"1113\",\"qty\":1,\"init_price\":\"5\"}}]', '5', '0', NULL, 'Smitesh More', '556665433', NULL, NULL, 'paid', NULL, '2020-01-30 13:42:54'),
(7, 'XYuIv5f3hJazD6yHoEmA', '2020-01-30', '202001307', '[{\"order\":{\"barcode\":\"1112\",\"qty\":2,\"init_price\":\"400\"}},{\"order\":{\"barcode\":\"1113\",\"qty\":1,\"init_price\":\"5\"}}]', '805', NULL, NULL, 'Sandesh', '809098098', NULL, 'cash', 'credit', NULL, '2020-01-30 13:53:28'),
(8, 'NMm5C23iEaLQYzs9P7wF', '2020-02-05', '202002058', '[{\"order\":{\"barcode\":\"1112\",\"qty\":1,\"init_price\":\"400\"}},{\"order\":{\"barcode\":\"1111\",\"qty\":1,\"init_price\":\"1000\"}}]', '2600', NULL, NULL, 'dsad', '8989898989', NULL, 'cheque', 'credit', NULL, '2020-02-05 08:55:36'),
(9, 'mBplfZP96j0nRAqv1T3W', '2020-02-07', '202002079', '[{\"order\":{\"barcode\":\"77961\",\"qty\":1,\"init_price\":\"400\"}}]', '400', NULL, NULL, 'Sandesh', '', NULL, 'cheque', 'paid', NULL, '2020-02-07 09:15:39'),
(10, 'MsqBt2S3Jyvj9T5z0fNQ', '2020-02-10', '2020021010', '[{\"order\":{\"barcode\":\"77961\",\"qty\":1,\"init_price\":\"400\"}}]', '400', NULL, NULL, '', '', NULL, NULL, 'paid', NULL, '2020-02-10 10:53:37');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_details`
--

CREATE TABLE `vendor_details` (
  `vendor_id` int(200) NOT NULL,
  `vendor_uid` varchar(200) DEFAULT NULL,
  `vendor_name` varchar(200) DEFAULT NULL,
  `vendor_phone1` varchar(200) DEFAULT NULL,
  `vendor_phone2` varchar(200) DEFAULT NULL,
  `vendor_address` varchar(400) DEFAULT NULL,
  `vendor_email_id` varchar(200) DEFAULT NULL,
  `vendor_gstno` varchar(200) DEFAULT NULL,
  `vendor_contact_person` varchar(200) DEFAULT NULL,
  `vendor_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `vendor_status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendor_details`
--

INSERT INTO `vendor_details` (`vendor_id`, `vendor_uid`, `vendor_name`, `vendor_phone1`, `vendor_phone2`, `vendor_address`, `vendor_email_id`, `vendor_gstno`, `vendor_contact_person`, `vendor_created_at`, `vendor_status`) VALUES
(2, '2PrGDanKvtRYLxSepjTi', 'suresh', '8896652365', '9965235212', 'safvxcvxfbxfbxvd,vdbviubvz.vnvndlviv', 'suresh@gmail.com', '2168331611651351', '9965859568', '2020-01-23 10:41:42', 1),
(3, 'z5V8thnKDOjGiUJB9PCA', 'kunal', '8852265326', '9965256325', 'efsvdsd,saihassa,sdniads.', 'kunal@gmail.com', '8865232145695874', '9965623568', '2020-01-23 10:43:48', 1),
(4, '1fpM6jScwglNFEZqGJd2', 'sxsxsx', '343434343', '4343434', 'kdcnjdsnckjsdncjnjkdc', 'dad@ssa.com', '333333', 'dwsdsd', '2020-01-30 12:37:43', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `credit_transactions`
--
ALTER TABLE `credit_transactions`
  ADD PRIMARY KEY (`ct_id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`login_id`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `vendor_details`
--
ALTER TABLE `vendor_details`
  ADD PRIMARY KEY (`vendor_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `credit_transactions`
--
ALTER TABLE `credit_transactions`
  MODIFY `ct_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `i_id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `s_id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `vendor_details`
--
ALTER TABLE `vendor_details`
  MODIFY `vendor_id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
