<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller{ 

  function index(){
    $this->load->model('inventory_model');
    $data['all_data'] = $this->inventory_model->search();
    foreach ($data['all_data'] as $key => $value) {
      $data['search_items'][] =$value['i_barcodeid'].':'. $value['i_product_name'];
    }
    $UserId = $this->inventory_model->getUserId();
    $now = date('Ymd');
    $data['invoice'] = $now.$UserId[0]['Userid'];
    $data['session_var'] = $this->session->userdata('login_company_name');
    $this->load->view('home_view',$data);
  }

  function save(){
    // $this->output->enable_profiler(TRUE);
    $this->load->model('inventory_model');
    $this->load->helper('string');
    $this->load->helper('url');
    $s_uid = random_string('alnum',20);
    $order = $this->input->post('final_order');
    $final = json_decode($order,TRUE);
    foreach ($final as $key => $value) {
      $infoObj[] = (object)[
        'order' =>[
          'barcode' => $value['barcode'],
          'qty' => $value['qty'],
          'init_price' => $value['init_price']
        ]
      ];
    }
    $infoData = json_encode($infoObj);
    $credit = $this->input->post('optradio1');
    if($credit == "paid"){
      $data = array (
        's_uid' => $s_uid,
        's_customer_name' => $this->input->post('customername'),
        's_date'=> $this->input->post('date'),
        's_invoice'=> $this->input->post('invoice'),
        's_phone'=> $this->input->post('phone'),
        's_payment_mode' => $this->input->post('optradio'),
        's_credit' => $credit,
        's_total' => $this->input->post('total'),
        's_remainig_payment' => 0,
        's_desc' => $infoData
      );
    }else{
      $data = array (
        's_uid' => $s_uid,
        's_customer_name' => $this->input->post('customername'),
        's_date'=> $this->input->post('date'),
        's_invoice'=> $this->input->post('invoice'),
        's_phone'=> $this->input->post('phone'),
        's_payment_mode' => $this->input->post('optradio'),
        's_credit' => $credit,
        's_total' => $this->input->post('total'),
        's_remainig_payment' => $this->input->post('total'),
        's_desc' => $infoData
      );
    }
    $inserted= $this->inventory_model->saveselldetails($data);
    $this->session->set_flashdata('response',"Sell Created Successfully");
    redirect('/Home');
  }
  function addsearchitem(){
    $this->load->model('inventory_model');
    $string = $this->input->post('additem');
    $additem=explode(":",$string);
    $barcode = $additem[0];
    $productname = $additem[1];
    $insert =$this->inventory_model->searchbox($barcode,$productname);
     echo json_encode($insert);
  }
  function Sales(){
    $this->load->model('inventory_model');
    $data['sales'] = $this->inventory_model->getAllSales();
    $this->load->view('sales_view',$data);
  }
  function Collection(){
    $this->load->model('inventory_model');
    $data['collection'] = $this->inventory_model->getRemainingCollection();
    $this->load->view('collection_view',$data);
  }


  function print_invoice(){
    $this->load->model('inventory_model');
    $this->load->helper('string');
    $this->load->helper('url');
    $s_uid = random_string('alnum',20);
    $order = $this->input->post('final_order');
    $final = json_decode($order,TRUE);
    foreach ($final as $key => $value) {
      $infoObj[] = (object)[
        'order' =>[
          'barcode' => $value['barcode'],
          'qty' => $value['qty'],
          'init_price' => $value['init_price']
        ]
      ];
    }
    $infoData = json_encode($infoObj);
    $data = array (
      's_uid' => $s_uid,
      's_customer_name' => $this->input->post('customername'),
      's_date'=> $this->input->post('date'),
      's_invoice'=> $this->input->post('invoice'),
      's_phone'=> $this->input->post('phone'),
      's_payment_mode' => $this->input->post('optradio'),
      's_credit' => $this->input->post('optradio1'),
      's_total' => $this->input->post('total'),
      's_desc' => $infoData
    );
    $inserted= $this->inventory_model->saveselldetails($data);
    // $this->session->set_flashdata('response',"Sell Created Successfully");
    // redirect('/Home');
    // $this->output->enable_profiler(TRUE);
    $this->load->model('inventory_model');
    $data['name'] = $this->input->post('customername');
    $data['date'] = $this->input->post('date');
    $data['invoice'] = $this->input->post('invoice');
    $data['phone'] = $this->input->post('phone');
    $order = $this->input->post('final_order');
    $data['order_before'] = json_decode($order,true);
    foreach ($data['order_before'] as $key => $value) {
      $barcode = $value['barcode'];
      $data['order'][] = $this->inventory_model->getProductDetailsViaBarcode($barcode);
      $data['order'][$key]['qty'] = $value['qty'];
    }
    $data['payment_mode'] = $this->input->post('optradio');
    $data['payment_status'] = $this->input->post('optradio1');
    $data['total'] = $this->input->post('total');
    // echo "<pre>"; print_r($data);exit;
    $this->load->view('print',$data);
  }


  function getSalesInBetween(){
    $start = $this->input->post('start');
    $end = $this->input->post('end');
    $this->load->model('inventory_model');
    $data = $this->inventory_model->getSalesInBetween($start,$end);
    echo json_encode($data);
  }

  function add_collection(){
    $this->load->helper('string');
    $this->load->model('inventory_model');
    $ct_uuid = random_string('alnum',20);
    $mode = $this->input->post("mode");
    $sell_id = $this->input->post('sell_id');
    $amount = $this->input->post('amount');
    if($mode == "cheque"){
      $data = array(
        'ct_uuid' => $ct_uuid,
        'ct_sales_uid' => $sell_id,
        'ct_amount' => $amount,
        'ct_payment_mode' => $mode,
        'ct_bank_name' => $this->input->post('bank_name'),
        'ct_cheque_no' => $this->input->post('cheque_number'),
        'ct_cheque_date' => $this->input->post('cheque_date')
      );
      $insert = $this->inventory_model->add_collection($data);
    }else{
      $data = array(
        'ct_uuid' => $ct_uuid,
        'ct_sales_uid' => $sell_id,
        'ct_amount' => $amount,
        'ct_payment_mode' => $mode
      );
      $insert = $this->inventory_model->add_collection($data);
    }
    $getCurrentAmount = $this->inventory_model->getCurrentAmount($sell_id);
    $newAmount = $getCurrentAmount[0]['remaining'] - $amount;
    $update = $this->inventory_model->update_final_amount($sell_id,$newAmount);
    echo '{"status" : "success"}';
  }


  function checkQuantity(){
    $this->load->model('inventory_model');
    $qty = $this->input->post('qty');
    $barcode = $this->input->post('barcode');
    $result = $this->inventory_model->getProductAllDetails($barcode);
    $currentQuantity = $result[0]['i_product_count'];
    if($qty > $currentQuantity){
      echo '{"status" : "no", "qty" : '.$result[0]['i_product_count'].' , "id" : '.$result[0]['i_barcodeid'].'}';
    }else{
      echo '{"status" : "yes", "qty" : '.$result[0]['i_product_count'].' , "id" : '.$result[0]['i_barcodeid'].'}';
    }
  }

  
}