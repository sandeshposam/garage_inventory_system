<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class StockManagement extends CI_Controller{ 
  function index(){
    $this->load->model('inventory_model');    
    $data['stockdata'] = $this->inventory_model->getStockdata();
    $data['action'] = "Addstock";
    $data['company'] = $this->session->userdata('login_company_name');
    $this->load->view('stockmanagement_view',$data);
  }
  function Addstock(){
    $this->load->model('inventory_model');
    $this->load->view('stockmanagement_view');
    $this->load->helper('string');
    $this->load->helper('url');
    $i_uid = random_string('alnum',36);
    $barcode = random_string('numeric',6);
    $data = array (
    'i_uid' => $i_uid,
    'i_barcodeid' => $barcode,
    'i_product_name' => $this->input->post('productname'),
    'i_product_category'=> $this->input->post('category'),
    'i_product_count'=> $this->input->post('quantity'),
    'i_product_description' => $this->input->post('description'),
    'i_product_rate' => $this->input->post('rate'),
    'i_product_rack' => $this->input->post('rack'),
    'i_product_total' => $this->input->post('amount'),
    'i_product_barcode' => $this->set_barcode($barcode)
   );
   $inserted= $this->inventory_model->addstock($data);
   $this->session->set_flashdata('response',"Stock Save Successfully");
   redirect('/StockManagement');
  }
 
  function update(){
    $this->load->model('inventory_model');
    $uuid = $this->uri->segment(3);
    $data['stock'] = $this->inventory_model->getstock($uuid);
    $data['stockdata'] = $this->inventory_model->getStockdata();
    $data['action'] = "updatestock";
    $this->load->view('stockmanagement_view',$data);
  
  }
  function updatestock(){
    $this->load->model('inventory_model');
    $this->load->view('stockmanagement_view');
    $this->load->helper('string');
    $this->load->helper('url');
      $i_uid = $this->input->post('productid');
    $data = array (
   
    'i_product_name' => $this->input->post('productname'),
    'i_product_category'=> $this->input->post('category'),
    'i_product_count'=> $this->input->post('quantity'),
    'i_product_description' => $this->input->post('description'),
    'i_product_rate' => $this->input->post('rate'),
    'i_product_rack' => $this->input->post('rack'),
    'i_product_total' => $this->input->post('amount')
   );
   $inserted= $this->inventory_model->updatestock($data,$i_uid);
   $this->session->set_flashdata('response',"Stock Edit Successfully");
   redirect('/StockManagement');
  
  }
  function To_purchase(){
    $this->load->model('inventory_model');    
    $data['purchasedata'] = $this->inventory_model->getpurchasedata();
  	$this->load->view('to_purchase_view',$data);
  }

  function uploadCsv(){
    $this->load->model('inventory_model'); 
    $fp = fopen($_FILES['file']['tmp_name'],'r') or die("can't open file");
    $i=0;
    while($csv_line = fgetcsv($fp,1024))
    {
      echo "<pre>";//print_r($csv_line);
      $main[$i]['Code'] = $csv_line[0];
      $main[$i]['Quality'] = $csv_line[1];
      $main[$i]['Category'] = $csv_line[2];
      $main[$i]['Make'] = $csv_line[3];
      $main[$i]['Description'] = $csv_line[4];
      $main[$i]['Model'] = $csv_line[5];
      $main[$i]['Quantity'] = $csv_line[6];
      $main[$i]['Mrp'] = $csv_line[7];
      $main[$i]['Rack'] = $csv_line[8];
      $i++;
    }
    $count = count($main);
    $this->load->helper('string');
    for($j = 1; $j < $count ; $j++){
      $barcode = random_string('numeric',6);
      $arr = array(
        'i_uid' => random_string('alnum',36),
        'i_barcodeid' => $barcode,
        'i_product_quality' => $main[$j]['Quality'],
        'i_product_name' => $main[$j]['Model'],
        'i_product_category' => $main[$j]['Category'],
        'i_product_description' => $main[$j]['Description'],
        'i_product_rate' => $main[$j]['Mrp'],
        'i_product_count' => $main[$j]['Quantity'],
        'i_product_manufacturer' => $main[$j]['Make'],
        'i_product_rack' => $main[$j]['Rack']
        // 'i_product_barcode' => $this->set_barcode($barcode)
      );
      // print_r($arr);
      // $this->inventory_model->insertData($arr);
    }
    // fclose($fp) or die("can't close file");
    // $data['success']="success";
    // print_r($main);

  }


  function set_barcode($code){
    $this->load->library('zend');
    $this->zend->load('Zend/Barcode');
    $file = Zend_Barcode::draw('code128', 'image', array('text' => $code), array());
    $code = time().$code;
    $store_image = imagepng($file,$_SERVER['DOCUMENT_ROOT']."/assets/barcode/{$code}.png");
    return $code.'.png';
  }
}