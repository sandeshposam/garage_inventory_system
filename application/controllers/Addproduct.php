<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Addproduct extends CI_Controller{ 
  function index(){
  	$this->load->model('inventory_model');
    $data['all_data'] = $this->inventory_model->search();
    foreach ($data['all_data'] as $key => $value) {
      $data['search_items'][] =$value['i_barcodeid'].':'. $value['i_product_name'];
    }
    $UserId = $this->inventory_model->getUserIdFromPurchase();
    $now = date('Ymd');
    $data['invoice'] = $now.$UserId[0]['Userid'];
    $data['vendors'] = $this->inventory_model->getAllVendorsToShow();
    $data['session_var'] = $this->session->userdata('login_company_name');
    $this->load->view('addproduct_view',$data);
  }

  function addNewItem(){
  	$this->load->model('inventory_model');
  	$string = $this->input->post('additem');
    $additem=explode(":",$string);
    $barcode = $additem[0];
    $productname = $additem[1];
    $insert =$this->inventory_model->searchbox($barcode,$productname);
	echo json_encode($insert);
  }

  function checkQuantity(){
  	$this->load->model('inventory_model');
  	$qty = $this->input->post('qty');
  	$barcode = $this->input->post('barcode');
  	$result = $this->inventory_model->getProductAllDetails($barcode);
  	$currentQuantity = $result[0]['i_product_count'];
  	if($qty > $currentQuantity){
  		echo '{"status" : "no", "qty" : '.$result[0]['i_product_count'].' , "id" : '.$result[0]['i_barcodeid'].'}';
  	}else{
  		echo '{"status" : "yes", "qty" : '.$result[0]['i_product_count'].' , "id" : '.$result[0]['i_barcodeid'].'}';
  	}
  }

  function save_order(){
  	$this->load->model('inventory_model');
  	$this->load->helper('string');
  	$this->load->helper('url');
  	$p_uid = random_string('alnum',20);
  	$po_order = $this->input->post('order_array');
    $final = json_decode($po_order,TRUE);
    $final_amount = 0;
    foreach ($final as $key => $value) {
    	$final_amount += $value['init_price'] * $value['qty']; 
      	$infoObj[] = (object)[
	        'order' =>[
	          'barcode' => $value['barcode'],
	          'qty' => $value['qty'],
	          'init_price' => $value['init_price']
	        ]
      	];
      	$update = $this->inventory_model->update_inventory($value['barcode'],$value['qty']);
    }
   
    $infoData = json_encode($infoObj);

    $payment_type = $this->input->post('payment_type');
    if($payment_type == "credit"){
    	$data = array (
	    	'p_uid' => $p_uid,
	    	'p_vendor_uid' => $this->input->post('vendor_name'),
	    	'p_date' => $this->input->post('date_received'),
	    	'p_order_no' => $this->input->post('po_no'),
	    	'p_current_date' => $this->input->post('current_date'),
	    	'p_payment_type' => $payment_type,
	    	'p_details' => $infoData,
	    	'p_total' => $final_amount,
	    	'p_remaining_amt' => $final_amount
	    );
    }else{
    	$data = array (
	    	'p_uid' => $p_uid,
	    	'p_vendor_uid' => $this->input->post('vendor_name'),
	    	'p_date' => $this->input->post('date_received'),
	    	'p_order_no' => $this->input->post('po_no'),
	    	'p_current_date' => $this->input->post('current_date'),
	    	'p_payment_type' => $payment_type,
	    	'p_details' => $infoData,
	    	'p_total' => $final_amount
	    );
    }
    
    $insert = $this->inventory_model->insert_purchase_order($data);
 	$this->session->set_flashdata('response',"PO Created Successfully");
    redirect('/Addproduct');
  	// $this->output->enable_profiler(TRUE);
  }

  function payout(){
  	$this->load->model('inventory_model');
  	$data['payout_data'] = $this->inventory_model->getAllPayout();
  	$this->load->view('payout_view',$data);
  }
}