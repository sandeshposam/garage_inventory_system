<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller{ 

	function index(){
 	$this->load->view('login');
	}

	function validate_email(){
		$data['email'] = $this->input->post('email');
		$this->load->model('login_model');
		$datas = $this->login_model->validate_email($data['email']);
		if ($datas->num_rows()>0){
			$data1 = $datas->row_array();
			// print_r($data1);
			// $this->session->set_flashdata('company' ,$data1['company_name']);
			$this->login_email($data1);
		}else{
			$this->register_email($data);
		}
	}

	function login_email($data){
		$this->load->view('login_email_view',$data);
	}

	function register_email($data){
		$this->load->view('register_email_view',$data);
	}

	function validate_login(){
		$data['companyname'] = $this->input->post('companyname');
		$data['email'] = $this->input->post('email');
		$data['password'] = $this->input->post('password');
		$this->load->model('login_model');
		$login = $this->login_model->validate_login($data['companyname'],$data['email'],$data['password'] );
		if ($login->num_rows()>0){
			$data1 = $login->row_array();
			$sesdata = array(
				'login_company_name' => $data['companyname'],
				'login_email_id' => $data['email'],
				'login_uuid' => $data1['login_uuid']
			);
			$this->session->set_userdata($sesdata);
			redirect('home');
		}else{
			$this->session->set_flashdata('response','Username or Password is Wrong');
        	redirect('login');
		}

	}

 	function register(){
 		$this->load->helper('string');
    	$this->load->helper('url');
 		$data['companyname'] = $this->input->post('companyname');
		$data['email'] = $this->input->post('email');
		$data['password'] = md5( $this->input->post('password'));
		
		$this->load->model('login_model');
		$login_uuid = random_string('alnum',20);
			$sesdata = array(
				'login_uuid' => $login_uuid,
				'login_company_name' => $data['companyname'],
				'login_email_id' => $data['email'],
				'login_password' => $data['password']
				
			);
			$login = $this->login_model->register_login($sesdata);
			$this->session->set_userdata($sesdata);
			redirect('home');
		
		}

		function logout(){
	      $this->session->sess_destroy();
	      redirect('login');
	  	}
	}