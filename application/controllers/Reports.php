<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reports extends CI_Controller{ 
  
  function sell(){
  	$this->load->view('sell_view');
  }
  function purchase(){
  	$this->load->view('purchase_view');
  }
  function customer(){
  	$this->load->view('customer_view');
  }
  function vendor(){
  	$this->load->view('vendor_view');
  }
}