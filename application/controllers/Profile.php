<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Profile extends CI_Controller{ 
	function index(){
		$this->load->model('login_model');
		// $data['profiledata'] = $this->login_model->getProfiledetails();		
		$data['companyname'] = $this->session->userdata('login_company_name');
		$data['email'] = $this->session->userdata('login_email_id');
		// $data['password'] = $this->session->userdata('login_password');
		$this->load->view('profile',$data);
		// $this->load->view('profile');
	}
}	