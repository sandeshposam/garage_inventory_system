<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AddVendor extends CI_Controller{ 
  function index(){
    $data['action'] = "save_vendor";
     $this->load->view('addvendor',$data);
    }
  function save_vendor(){
  	$this->load->model('inventory_model');
  	 $this->load->helper('string');
    $this->load->helper('url');

    $vendor_uid = random_string('alnum',20);
    $data = array (
      'vendor_uid' => $vendor_uid,
      'vendor_name' => $this->input->post('vendorname'),
      'vendor_phone1'=> $this->input->post('phone1'),
      'vendor_phone2'=> $this->input->post('phone2'),
      'vendor_address'=> $this->input->post('address'),
      'vendor_email_id' => $this->input->post('emailid'),
      'vendor_gstno' => $this->input->post('gstno'),
      'vendor_contact_person' => $this->input->post('contactperson')
    );
    $inserted= $this->inventory_model->savevendordetails($data);
    $this->session->set_flashdata('response',"vendor save Successfully");
    redirect('/AddVendor/view');

  }
  function view(){
  	
  	$this->load->model('inventory_model');    
    $data['vendordata'] = $this->inventory_model->getVendordetails();
    $data['action'] = "save_vendor";
    $this->load->view('viewvendor',$data);
  }
  function edit(){
  	$this->load->model('inventory_model');
  	$uuid = $this->uri->segment(3);
  	$data['vendor'] = $this->inventory_model->getvendor($uuid);
    $data['action'] = "updatevendor";
  	$this->load->view('addvendor',$data);
  }
  function updatevendor(){
    $this->load->model('inventory_model');
    $vendor_uid = $this->input->post('vendorid');
    $data = array (
      'vendor_name' => $this->input->post('vendorname'),
      'vendor_phone1'=> $this->input->post('phone1'),
      'vendor_phone2'=> $this->input->post('phone2'),
      'vendor_address'=> $this->input->post('address'),
      'vendor_email_id' => $this->input->post('emailid'),
      'vendor_gstno' => $this->input->post('gstno'),
      'vendor_contact_person' => $this->input->post('contactperson')
    );
    $inserted= $this->inventory_model->updatevendordetails($data,$vendor_uid);
    redirect('/AddVendor/view');

  }
}