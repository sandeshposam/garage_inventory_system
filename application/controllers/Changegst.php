<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Changegst extends CI_Controller{ 
	function index(){
		$this->load->view('changegst_view');
	}
	function gstsetting(){
		$this->load->model('login_model');
	  	$this->load->helper('string');
	    $this->load->helper('url');
	    $data = array (
	     
	      'gst_type' => $this->input->post('gst'),
	      'gst_percentage'=> $this->input->post('gstpercentage')
	     
	    );
	    $inserted= $this->login_model->savegstsetings($data);
	    $this->session->set_flashdata('response',"GST Setting Change Successfully");
	    redirect('Changegst/');
  	}
}