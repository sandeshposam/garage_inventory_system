<?php
class inventory_model extends CI_Model{
	function saveselldetails($data){
		$this->db->insert('sales',$data);
	}
	function search(){
		$query = $this->db->query("SELECT * from inventory");
		return $query->result_array();
	}
	function searchbox($barcode,$productname){
		$query = $this->db->query("SELECT * from inventory where i_barcodeid ='$barcode' and i_product_name='$productname'");
		return $query->result_array();
	}
	function addstock($data){
		$this->db->insert('inventory',$data);
	}
	function getStockdata(){
		$query = $this->db->query("SELECT * from  inventory ORDER BY ABS(i_product_count)");
		return $query->result_array();
	}
	function getproductdetails($barcode){
		$query =$this->db->query("SELECT * from inventory where i_barcodeid = '$barcode'");
		return $query->result_array();
	}
	function savevendordetails($data){
		$this->db->insert('vendor_details',$data);
	}	
	function getVendordetails(){
		$query = $this->db->query("SELECT  * from  vendor_details ");
		return $query->result_array();
	}
	function getvendor($uuid){
		$query = $this->db->query("SELECT * from vendor_details where vendor_uid = '$uuid'");
		return $query->result_array();
        
	}
	function getstock($uuid){
		$query = $this->db->query("SELECT * from inventory where i_uid = '$uuid'");
		return $query->result_array();
	}


	function getUserId(){
		$query = $this->db->query("SELECT MAX(s_id) + 1 AS Userid FROM sales");
		return $query->result_array();
	}

	function getUserIdFromPurchase(){
		$query = $this->db->query("SELECT MAX(p_uid) + 1 AS Userid FROM purchase");
		return $query->result_array();
	}

	function getProductDetailsViaBarcode($barcode){
		$query = $this->db->query("SELECT * from inventory where i_barcodeid = '$barcode' ");
		$data = $query->result_array();
		return $data[0];
	}

	function getAllSales(){
		$query = $this->db->query("SELECT * FROM sales");
		return $query->result_array();
	}

	function getSalesInBetween($start,$end){
		$query = $this->db->query("SELECT * FROM sales WHERE DATE(s_created_at) > '$start' AND DATE(s_created_at) <= '$end'");
		return $query->result_array();
	}

	function getRemainingCollection(){
		$query = $this->db->query("SELECT * FROM sales WHERE s_credit = 'credit' AND s_remainig_payment > 0");
		return $query->result_array();
	}

	function getCurrentAmount($suid){
		$query = $this->db->query("SELECT s_remainig_payment as remaining FROM sales WHERE s_uid = '$suid'");
		return $query->result_array();
	}

	function add_collection($data){
		$this->db->insert('credit_transactions',$data);
	}

	function update_final_amount($sell_id,$newAmount){
		$query = $this->db->query("UPDATE sales SET s_remainig_payment = '$newAmount' WHERE s_uid = '$sell_id'");
		return true;
	}
	function updatestock($data,$i_uid){
		$this->db->where('i_uid',$i_uid);
		 $this->db->update('inventory',$data);
		 return true;
	}
	function updatevendordetails($data,$vendor_uid){
		$this->db->where('vendor_uid',$vendor_uid);
		 $this->db->update('vendor_details',$data);				
		 return true;
	}
	function getpurchasedata(){
		// ORDER BY i_product_count ASC
		$query = $this->db->query("SELECT  * from  inventory  ORDER BY ABS(i_product_count)");
		return $query->result_array();
	}

	function getProductAllDetails($barcode){
		$query = $this->db->query("SELECT * FROM inventory WHERE i_barcodeid = '$barcode'");
		return $query->result_array();
	}
	
	function getAllVendorsToShow(){
		$query = $this->db->query("SELECT * FROM vendor_details");
		return $query->result_array();
	}

	function update_inventory($barcode,$qty){
		$query = $this->db->query("SELECT * FROM inventory WHERE i_barcodeid = '$barcode'");
		$data = $query->result_array();
		$currentQty = $data[0]['i_product_count'];
		$newQty = $currentQty + $qty;
		$update = $this->db->query("UPDATE inventory SET i_product_count = '$newQty' WHERE i_barcodeid = '$barcode'");
		return true;
	}

	function insert_purchase_order($data){
		$this->db->insert('purchase',$data);
	}

	function getAllPayout(){
		$query = $this->db->query("SELECT * FROM purchase p JOIN vendor_details v ON p.p_vendor_uid = v.vendor_uid WHERE p.p_payment_type = 'credit' AND p.p_remaining_amt > 0");
		return $query->result_array();
	}

	function insertData($data){
		$this->db->insert('inventory',$data);
	}
}