
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/bootstrap.css';?>" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/style.css';?>" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/font-awesome.css';?>" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href="<?php echo INCLUDE_PATH_CSS.'/SidebarNav.min.css';?>" media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="<?php echo INCLUDE_PATH_JS.'/jquery-1.11.1.min.js';?>"></script>
<!-- <script src="<?php echo INCLUDE_PATH_JS.'/modernizr.custom.js';?>"></script> -->

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<!-- <script src="<?php echo INCLUDE_PATH_JS.'/Chart.js';?>"></script> -->
<!-- //chart -->

<!-- Metis Menu -->
<script src="<?php echo INCLUDE_PATH_JS.'/metisMenu.min.js';?>"></script>
<script src="<?php echo INCLUDE_PATH_JS.'/custom.js';?>"></script>
<link href="<?php echo INCLUDE_PATH_CSS.'/custom.css';?>" rel="stylesheet">
<!--//Metis Menu -->
<!-- search box jquery -->

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.sell-head{
  padding: 5px 0em;
    background-color: #f5f5f5;
    border-bottom: 1px solid #ddd;
}
#form {
    /*background-color: #FFF;*/
    /*height: 600px;*/
    width: 600px;
    /*margin-right: auto;*/
    /*margin-left: 10em;*/
    /*margin-top: 0px;*/
    /*border-top-left-radius: 10px;*/
    /*border-top-right-radius: 10px;*/
    padding: 0px;
    /*text-align:center;*/
    float: right;

}
#cal{
    font-family: Georgia, "Times New Roman", Times, serif;
    font-size: 14px;
    color: #333;
    height: 20px;
    width: 200px;
    margin-top: 10px;
    margin-left: 10px;
    text-align: right;
    margin-right:15px;
    float:left;
}
 a.btn {
     -webkit-transform: scale(0.8);
     -moz-transform: scale(0.8);
     -o-transform: scale(0.8);
     -webkit-transition-duration: 0.5s;
     -moz-transition-duration: 0.5s;
     -o-transition-duration: 0.5s;
 }
</style>
<!--pie-chart --><!-- index page sales reviews visitors pie chart -->
<script src="<?php echo INCLUDE_PATH_JS.'/pie-chart.js';?>" type="text/javascript"></script>
 

</head> 
<body class="cbp-spmenu-push">
  <div class="main-content">
   <?php $this->load->view('includes/side_navbar'); ?>
    <!--left-fixed -navigation-->
    
    <!-- header-starts -->
   
    <!-- //header-ends -->
    <!-- main content start-->
    <div id="page-wrapper">
      <div class="main-page">
        <div class="inline-form widget-shadow">
            <div class="form-title">
              <h4 style="text-align: center;font-weight: bold;">GST Details</h4>
            </div>
            <form method="POST" action="<?php echo APP_PATH.'Changegst/gstsetting'; ?>" id="savegstdetails" >
              <br>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                  <label for="sel1">Select GST Type:</label>
                        <select class="" id="sel1" name="gst" style="width:60%;">
                          <option disabled selected>Select Tax Type</option>
                          <option value="">Inclusive GST</option>
                          <option value="">Exclusive GST</option>
                        </select>
                      </div>
                </div>
                <div class="col-sm-6">
                   <label style="padding-left: 15px;">GST % :</label>
                    <input type="text" name="gstpercent" value="" required>
                </div>
              </div>
            <br/>
            <div class="row" style="text-align: center;"> 
              <!-- <div class="col-sm-12" > -->
                <div class="col-sm-6">
                  <button  class="btn btn-default" style="width: 100px;" onclick="submit();">Save</button>
                </div>
                <div class="col-sm-6">
                  <button type="submit" class="btn btn-default" style="width: 100px;" onclick="location.reload()">Cancel</button>
                </div>
              <!-- </div> -->
            </div><br/>
            </form>
          </div>        
      </div>
    </div>
  <!--footer-->
  <?php $this->load->view('includes/footer'); ?>
    <!--//footer-->
  </div>
    
  <!-- new added graphs chart js-->
  
    <!-- <script src="<?php //echo INCLUDE_PATH_JS.'/Chart.bundle.js';?>"></script> -->
    <!-- <script src="js/utils.js"></script> -->
  
  
  <!-- new added graphs chart js-->
  
  <!-- Classie --><!-- for toggle left push menu script -->
    <!-- <script src="js/classie.js"></script> -->
    <script>     
       function submit(){
    $("#savegstdetails").submit();
   }

   
    </script>
  <!-- //Classie --><!-- //for toggle left push menu script -->
    
  <!--scrolling js-->
  <script src="<?php echo INCLUDE_PATH_JS.'/jquery.nicescroll.js';?>"></script>
  <script src="<?php echo INCLUDE_PATH_JS.'/scripts.js';?>"></script>
  <!--//scrolling js-->
  
  <!-- side nav js -->
  <script src="<?php echo INCLUDE_PATH_JS.'/SidebarNav.min.js';?>" type='text/javascript'></script>
  <script>
      $('.sidebar-menu').SidebarNav()
    </script>
 
  
  
  <!-- Bootstrap Core JavaScript -->
   <script src="<?php echo INCLUDE_PATH_JS.'/bootstrap.js';?>"> </script>
  <!-- //Bootstrap Core JavaScript -->
  
</body>
</html>