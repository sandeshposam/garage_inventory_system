
<!DOCTYPE HTML>
<html>
<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href="<?php echo INCLUDE_PATH_CSS.'/bootstrap.css';?>" rel='stylesheet' type='text/css' />

    <!-- Custom CSS -->
    <link href="<?php echo INCLUDE_PATH_CSS.'/style.css';?>" rel='stylesheet' type='text/css' />
    <link href="<?php echo INCLUDE_PATH_CSS.'/font-awesome.css';?>" rel="stylesheet"> 
    <link href="<?php echo INCLUDE_PATH_CSS.'/SidebarNav.min.css';?>" media='all' rel='stylesheet' type='text/css'/>
    <script src="<?php echo INCLUDE_PATH_JS.'/jquery-1.11.1.min.js';?>"></script>
    <link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo INCLUDE_PATH_JS.'/swal.min.js';?>"></script>
    <script src="<?php echo INCLUDE_PATH_JS.'/pie-chart.js';?>" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.min.js"></script>
</head> 
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8" style="border : 1px solid black;">
                <div id="main_print">
                    <div style="text-align: center;"><h2>Order Invoice</h2></div>
                    <br><br>
                    <!-- <hr> -->
                    <div class="main">
                        <div class="row">
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6" >
                                <div class="table-responsive">
                                    <table style="float: right;font-weight: 900;">
                                        <tr>
                                            <td>Invoice Number</td>
                                            <td>: <?php echo $invoice;?></td>
                                        </tr>
                                        <tr>
                                            <td>Date</td>
                                            <td>: <?php echo $date;?></td>
                                        </tr>
                                        <tr>
                                            <td>Name</td>
                                            <td>: <?php echo $name;?></td>
                                        </tr>
                                        <tr>
                                            <td>Phone</td>
                                            <td>: +91 <?php echo $phone;?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br>
                        <!-- <hr> -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <!-- <th>Barcode No</th> -->
                                                <th>Product Name</th>
                                                <th>Quantity</th>
                                                <th>Unit Price</th>
                                                <th>Line Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $i = 1;
                                            foreach ($order as $key => $value) {
                                            ?>
                                            <tr>
                                                <td style="text-align: right;"><?php echo $i;?></td>
                                                <!-- <td><?php echo $value['i_barcodeid'];?></td> -->
                                                <td style="text-align: right;"><?php echo $value['i_product_name'];?></td>
                                                <td style="text-align: right;"><?php echo $value['qty'];?></td>
                                                <td style="text-align: right;"><?php echo $value['i_product_rate'];?></td>
                                                <td style="text-align: right;"><?php $lineTotal = ($value['i_product_rate'] * $value['qty']); echo $lineTotal;?></td>
                                            </tr>
                                            <?php 
                                                $i++;
                                            } 
                                            ?>
                                            <!-- <tr style="text-align: right;">
                                                <td colspan="4"><b>Total</b></td>
                                                <td><b><?php //echo $total;?></b></td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br>
                        <!-- <hr> -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6" style="text-align: left;">
                                    <label>Payment Type : <?php echo $payment_mode;?></label>
                                    <br>
                                    <label>Payment Status : <?php echo $payment_status;?></label>
                                </div>
                                <div class="col-md-6" style="text-align: right;">
                                    <label>Discount : <?php  echo isset($discount) ? $discount : "0%";?></label><br>
                                    <label>Tax : <?php echo isset($tax) ? "Rs. ".$tax : "Rs. 0.00";?></label><br>
                                    <label>Final Total : Rs. <?php echo $total;?>.00</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="editor"></div>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        
                        <a href="<?php echo APP_PATH.'Home';?>" class="btn btn-warning">Back</a>
                        <button class="btn btn-primary" onclick="print_page();">Print</button>
                        <!-- <button class="btn btn-warning" id="cmd">Download PDF</button> -->
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
    <script src="<?php echo INCLUDE_PATH_JS.'/jquery.nicescroll.js';?>"></script>
    <script src="<?php echo INCLUDE_PATH_JS.'/scripts.js';?>"></script>
    <script src="<?php echo INCLUDE_PATH_JS.'/SidebarNav.min.js';?>" type='text/javascript'></script>

    <script src="<?php echo INCLUDE_PATH_JS.'/SimpleChart.js';?>"></script>

    <script src="<?php echo INCLUDE_PATH_JS.'/bootstrap.js';?>"> </script>
    <script type="text/javascript">
        function print_page(){
            w=window.open();
            w.document.write($('#main_print').html());
            w.print();
            w.close();
        }
    </script>
</body>
</html>