
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/bootstrap.css';?>" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/style.css';?>" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/font-awesome.css';?>" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href="<?php echo INCLUDE_PATH_CSS.'/SidebarNav.min.css';?>" media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="<?php echo INCLUDE_PATH_JS.'/jquery-1.11.1.min.js';?>"></script>
<!-- <script src="<?php //echo INCLUDE_PATH_JS.'/modernizr.custom.js';?>"></script> -->

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<!-- <script src="<?php echo INCLUDE_PATH_JS.'/Chart.js';?>"></script> -->
<!-- //chart -->

<!-- Metis Menu -->

<link href="<?php echo INCLUDE_PATH_CSS.'/custom.css';?>" rel="stylesheet">
<!--//Metis Menu -->
<!-- search box jquery -->

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript" src="<?php echo INCLUDE_PATH_JS.'/swal.min.js';?>"></script>
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.sell-head{
  padding: 5px 0em;
    background-color: #f5f5f5;
    border-bottom: 1px solid #ddd;
}
#form {
    /*background-color: #FFF;*/
    /*height: 600px;*/
    width: 600px;
    /*margin-right: auto;*/
    /*margin-left: 10em;*/
    /*margin-top: 0px;*/
    /*border-top-left-radius: 10px;*/
    /*border-top-right-radius: 10px;*/
    padding: 0px;
    /*text-align:center;*/
    /*float: right;*/

}
.cal{
    font-family: Georgia, "Times New Roman", Times, serif;
    font-size: 14px;
    color: #333;
    height: 20px;
    width: 200px;
    margin-top: 10px;
    margin-left: 10px;
    text-align: right;
    margin-right:15px;
    float:left;
}
 a.btn {
     -webkit-transform: scale(0.8);
     -moz-transform: scale(0.8);
     -o-transform: scale(0.8);
     -webkit-transition-duration: 0.5s;
     -moz-transition-duration: 0.5s;
     -o-transition-duration: 0.5s;
 }
 .line{
     margin-top: 5px;
    margin-bottom: 5px;
    border-top: 1px solid #eee;
  }
  .btn-default {
  background-color: #629aa9;  
  border-radius: 5px;
  color: white;
  /*padding: .5em;*/
  /*text-decoration: none;*/
}
</style>
<!--pie-chart --><!-- index page sales reviews visitors pie chart -->
<script src="<?php echo INCLUDE_PATH_JS.'/pie-chart.js';?>" type="text/javascript"></script>
 
<!-- //pie-chart --><!-- index page sales reviews visitors pie chart -->

  <!-- requried-jsfiles-for owl -->
          <link href="<?php echo INCLUDE_PATH_CSS.'/owl.carousel.css';?>" rel="stylesheet">
          <script src="<?php echo INCLUDE_PATH_JS.'/owl.carousel.js';?>"></script>
            <script>
              $(document).ready(function() {
                $("#owl-demo").owlCarousel({
                  items : 3,
                  lazyLoad : true,
                  autoPlay : true,
                  pagination : true,
                  nav:true,
                });
              });
            </script>
          <!-- //requried-jsfiles-for owl -->
</head> 
<body class="cbp-spmenu-push">
  <div class="main-content">
   <?php $this->load->view('includes/side_navbar'); ?>
    <!--left-fixed -navigation-->
    
    <!-- header-starts -->
   
    <!-- //header-ends -->
    <!-- main content start-->

    <div id="page-wrapper">
      <div class="main-page">
        <div class="inline-form widget-shadow " style="margin-top: 0em!important;margin-bottom: 0em!important;">
            <div class="form-title" style="padding: 0.5em 0em!important;height: 112px;">
              <form method="POST" action="<?php echo APP_PATH.'Home/save'?>" id="savecustomerdetails">
              <input type="hidden" name="final_order" id="final_order" value="">
              <input type="hidden" name="total" id="main_total" value="">
              <div class="row" id="row1">
                <div class="col-sm-3" style="margin: 0 0 0.5em 0!important;">
                  <label>Customer Name:</label><input type="text" class="form-group" name="customername" value="">
                  <!-- <h4 style="font-weight: bold;">Sell Products:</h4> -->
                </div>
                <div class="col-sm-3">
                 <label>Date:</label><br><input type="date" id="current_date" class="form-group" name="date" style="line-height: 15px;" required>
                </div>
                <div class="col-sm-3">
                  <label>Invoice Number:</label><input type="text" class="form-group" name="invoice" value="<?php echo $invoice;?>" required >
                </div>
                <div class="col-sm-3">
                 <label>Phone Number:</label><input type="number" class="form-group" name="phone" required>
                </div>
              </div>
            
                <div class="row" id="row1" >
                <div class="col-sm-12">
                  <div class="ui-widget">
                    <label for="tags">Search Products: </label>
                    <input id="tags" name="tags" placeholder="Search by Product Code" onkeyup="checkPrintOption(this)" class="" style="width: 50%;" required>
                  </div>
                </div>
              </div>
              <div id="result"></div>
            </div>

            <!-- <div class="sell-head">
              <div class="row">
                <div class="col-sm-12">
                  <div class="ui-widget">
                    <label for="tags">Search Products: </label>
                    <input id="tags">
                  </div>
                </div>
              </div>
            </div> -->
            <div class="form-body">
              <div data-example-id="simple-form-inline" style="overflow-x:scroll!important; height: 280px;"> 
                <!-- <form>  -->
                  <table class="table table-bordered" >
                    <thead>
                      <tr>
                        <th>Sr No.</th>
                        <th>Product Code</th>
                        <th>Product Name</th>                        
                        <th>Quantity</th>
                        <th>Amount</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="tablecontent">
                      
                    </tbody>
                  </table>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-6">
                    <div class="row">
                      <table>
                        <th><center></center></th>
                        <tr>
                          <td>
                            <div class="form-check">
                              <label class="form-check-label">Payment Mode:&nbsp;
                              <input type="radio" class="form-check-input" name="optradio" value="cheque" >&nbsp;Cheque
                            </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-check" style="margin-left: 1em;">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" value="cash" name="optradio" >&nbsp;Cash
                              </label>
                            </div>
                          </td>
                          <td>
                            <div class="form-check" style="margin-left: 1em;">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" value="cash" name="optradio" >&nbsp;Card
                              </label>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="row" style="margin-left: 90px;">
                      <table>
                        <th><center></center></th>
                          <tr>
                           <td>
                            <div class="form-check">
                              <label class="form-check-label">Payment Type:&nbsp;
                                <input type="radio" class="form-check-input" name="optradio1" value="paid" checked>&nbsp;Paid
                              </label>
                            </div>
                           </td>
                           <td>
                             <div class="form-check" style="margin-left: 1em;">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" value="credit" name="optradio1">&nbsp;Credit
                              </label>
                            </div>
                           </td>
                         </tr>
                       </table>
                    </div>
                  </div>
                </div>
                
                  <hr class="line">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="row" >
                  <div >
                    <div class="col-sm-4">
                      <a type="submit" class="btn btn-default move_to">Save & Print</a>
                    </div>
                    <div class="col-sm-4">
                      <a type="button" href="#" class="btn btn-default" role="button" style="width: 100px;" onclick="submit();">Save</a>
                    </div>
                    <div class="col-sm-4">
                      <a class="btn btn-default" style="width: 100px;" onclick="location.reload()">Cancel</a>
                    </div>
                  </div>
                </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="row">
                  <div id="form" >                 
                    <fieldset >
                      <!-- <div class="col-sm-3"> -->
                        <div class="form-group">
                          <label for="" class ="cal">Discount%:</label>
                          <input name="" type="number" style="width: 100px;" disabled="" />
                        </div>
                      <!-- </div> -->
                      <!-- <div class="col-sm-3"> -->
                        <div class="form-group">
                          <label for="" class ="cal">Taxes:</label>
                          <input name="" type="number" style="width: 100px;" disabled="" />
                        </div>
                      <!-- </div> -->
                      <!-- <div class="col-sm-3"> -->
                        <div class="form-group">
                          <label for="" class ="cal" >Total:</label>
                          <input name="total" id="total" type="number" style="width: 100px;" disabled="" />
                        </div>
                      <!-- </div> -->
                    </fieldset>
                   </div>
                </div>
                  </div>
                </div>
                  </form>
                </div>
                 <div class="clearfix"></div>
              </div>
            </div>
          </div>        
      </div>
    </div>
  <!--footer-->
  
    <!--//footer-->
  </div>
<form>
  <input type="hidden" name="" value="">
</form>
  <!-- new added graphs chart js-->
  
    <!-- <script src="<?php //echo INCLUDE_PATH_JS.'/Chart.bundle.js';?>"></script> -->
    <!-- <script src="js/utils.js"></script> -->
  <script type="text/javascript">
    $(document).ready(function() {
        <?php if($this->session->flashdata('response')){ ?>
          swal({
            title: "<?php echo $this->session->flashdata('response'); ?>",
            icon: "success",
          });
        <?php } ?>
        document.getElementById('current_date').valueAsDate = new Date();
    });
    $(document).ready(function(){
      if(localStorage.getItem("order") != null){
        localStorage.removeItem("order");
      }
    })
         $( function() {
          var availableTags = [<?php echo '"'.implode('","',  $search_items ).'"' ?>];
          $( "#tags" ).autocomplete({
            source: availableTags,
            select: function(event, ui) {
              $.ajax({
                  url: "<?php echo APP_PATH.'Home/addsearchitem';?>", 
                  data: {additem:ui.item.value},
                  method: "POST",
                  dataType: "json",
                  success: function(result){
                    if(localStorage.getItem("order") === null){
                      var order = [{"barcode" : result[0].i_barcodeid, "qty" : 1,"init_price" : result[0].i_product_rate}];
                      localStorage.setItem("order",JSON.stringify(order));
                      var session_string = '';
                      if(result != null || result != ''){                                       
                        session_string ='<tr id="row_'+result[0].i_barcodeid+'">'+
                        '<td>'+result[0].i_id+'</td>'+
                          '<td>'+result[0].i_barcodeid+'</td>'+
                          '<td>'+result[0].i_product_name+'</td>'+                        
                          '<td> <input type="text" name="" value="1" style="width: 70px; border:none;" id="qty_'+result[0].i_barcodeid+'" onkeyup="multiply(this);" data-price="'+result[0].i_product_rate+'" data-id="'+result[0].i_id+'" data-barcode = "'+result[0].i_barcodeid+'"></td>'+
                          '<td class="itemtotal"  id="'+result[0].i_id+'">'+result[0].i_product_rate+'</td>'+
                          '<td><a href="#" class="btn btn-primary a-btn-slide-text" onclick="remove(this);" data-barcode = '+result[0].i_barcodeid+'>'+
                            '<span class="fa fa-times" aria-hidden="true"></span>'+
                            '<span><strong>Remove</strong></span> </a>'+             
                            '</a>'+
                          '</td>'+
                          '</tr>';
                          $("#tablecontent").append(session_string);
                          
                      }
                      calculatetotal();
                    }else{
                      var order = localStorage.getItem("order");
                      var jsonOrder = JSON.parse(order);
                      if(_isContains(jsonOrder,result[0].i_barcodeid)){
                        $.each(jsonOrder, function(index,value){
                          if(value.barcode == result[0].i_barcodeid){
                            var pre = value.qty;
                            var newq = Number(pre) + 1;
                            value.qty = newq;
                            var current_price = value.init_price;
                            var new_price = newq * Number(current_price);
                            $("#"+result[0].i_id).text(new_price);
                            $("#qty_"+result[0].i_barcodeid).val(newq);

                          }
                        });
                        localStorage.setItem("order",JSON.stringify(jsonOrder));
                        calculatetotal();
                      }else{
                        var order = {"barcode" : result[0].i_barcodeid, "qty" : 1,"init_price" : result[0].i_product_rate};
                        jsonOrder.push(order);
                        localStorage.setItem("order",JSON.stringify(jsonOrder));
                        if(result != null || result != ''){                                       
                          session_string ='<tr id="row_'+result[0].i_barcodeid+'">'+
                          '<td>'+result[0].i_id+'</td>'+
                            '<td>'+result[0].i_barcodeid+'</td>'+
                            '<td>'+result[0].i_product_name+'</td>'+                        
                            '<td> <input type="text" name="" value="1" style="width: 70px; border:none;" id="qty_'+result[0].i_barcodeid+'" onkeyup="multiply(this);" data-price="'+result[0].i_product_rate+'" data-id="'+result[0].i_id+'"></td>'+
                            '<td class="itemtotal"  id="'+result[0].i_id+'">'+result[0].i_product_rate+'</td>'+
                            '<td><a href="#" class="btn btn-primary a-btn-slide-text" onclick="remove(this);" data-barcode = '+result[0].i_barcodeid+'>'+
                              '<span class="fa fa-times" aria-hidden="true"></span>'+
                              '<span><strong>Remove</strong></span> </a>'+             
                              '</a>'+
                            '</td>'+
                            '</tr>';
                            $("#tablecontent").append(session_string);
                            $("#tags").val('');
                        }
                        calculatetotal();
                      }

                    }

                    $("#tags").val('');
                    
                  }
              });
              
            }
          });
        });
        function _isContains(json, value) {
          let contains = false;
          Object.keys(json).some(key => {
              contains = typeof json[key] === 'object' ? _isContains(json[key], value) : json[key] === value;
               return contains;
          });
           return contains;
        }
         // delete row
          // function deleterow(btn) {
          //   var row = btn.parentNode.parentNode;
          //   row.parentNode.removeChild(row);
          // }
          // add Quantity and amount increase
         function multiply(el)
          {
            var qty = $(el).val();
            var price = $(el).attr("data-price");
            var total = Number(qty)*Number(price);
            var id = $(el).attr("data-id");
            var barcode = $(el).attr("data-barcode");
            checkQuantity(qty,barcode);
            $("#"+id).text(total);
          }
             // add total amount
            function calculatetotal(){
              var values = [];
              $('.itemtotal').each(function(){
                  values.push(Number($(this).text())); 
              });
              var total = values.reduce(myFunc);
              $("#total").val(total);
              $("#main_total").val(total);
            }

             function myFunc(total, num) {
                return total + num;
              }
    </script>
  <script>
   
   function submit(){
    var finalOrder = localStorage.getItem("order");
    $("#final_order").val(finalOrder);
    $("#savecustomerdetails").submit();
   }

   // invoice print
   $(".move_to").on("click", function(e){
    var finalOrder = localStorage.getItem("order");
    $("#final_order").val(finalOrder);
     e.preventDefault();
    $('#savecustomerdetails').attr('action', "<?php echo APP_PATH.'Home/print_invoice';?>").submit();
   });

   function remove(el){
      var barcode = $(el).attr('data-barcode');
      $("#row_"+barcode).remove();
      var finalOrder = localStorage.getItem("order");
      var jsonOrder = JSON.parse(finalOrder);
      var len = jsonOrder.length;
      var newArr = [];
      if(len > 1){
        $.each(jsonOrder, function(index,value){
          if(value.barcode == barcode){
          }else{
            newArr.push(value);
          }
        });
        localStorage.setItem("order",JSON.stringify(newArr));
      }else{
        localStorage.removeItem("order");
      }
      
    }

    function checkPrintOption(el){
      var code = $(el).val();
      
    }

    function checkQuantity(qty,barcode){
      $.ajax({
        url: "<?php echo APP_PATH.'Home/checkQuantity';?>", 
        data: { qty : qty , barcode : barcode},
        method: "POST",
        dataType: "json",
        success: function(result){
          console.log(result);
          if(result.status == "yes"){
            return true;
          }else{
            swal({
              title: "Item not available!",
              icon: "error",
            }).then(function(){
              $("#qty_"+result.id).val(result.qty);
            });
            
          }
        }
      });
    }

    </script>
  <!-- new added graphs chart js-->
  
  <!-- Classie --><!-- for toggle left push menu script -->
    <script src="js/classie.js"></script>
    
  <!-- //Classie --><!-- //for toggle left push menu script -->
    
  <!--scrolling js-->
  <script src="<?php echo INCLUDE_PATH_JS.'/jquery.nicescroll.js';?>"></script>
  <script src="<?php echo INCLUDE_PATH_JS.'/scripts.js';?>"></script>
  <!--//scrolling js-->
  
  <!-- side nav js -->
  <script src="<?php echo INCLUDE_PATH_JS.'/SidebarNav.min.js';?>" type='text/javascript'></script>
  <script>
      $('.sidebar-menu').SidebarNav()
    </script>
  <!-- //side nav js -->
  
  <!-- for index page weekly sales java script -->
  <script src="<?php echo INCLUDE_PATH_JS.'/SimpleChart.js';?>"></script>
   
  
  <!-- //for index page weekly sales java script -->
  
  
  <!-- Bootstrap Core JavaScript -->
   <script src="<?php echo INCLUDE_PATH_JS.'/bootstrap.js';?>"> </script>
  <!-- //Bootstrap Core JavaScript -->
  
</body>
</html>