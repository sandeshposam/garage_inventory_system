<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/bootstrap.css';?>" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/style.css';?>" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/font-awesome.css';?>" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href="<?php echo INCLUDE_PATH_CSS.'/SidebarNav.min.css';?>" media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="<?php echo INCLUDE_PATH_JS.'/jquery-1.11.1.min.js';?>"></script>
<!-- <script src="<?php echo INCLUDE_PATH_JS.'/modernizr.custom.js';?>"></script> -->

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<!-- <script src="<?php echo INCLUDE_PATH_JS.'/Chart.js';?>"></script> -->
<!-- //chart -->

<!-- Metis Menu -->
<script src="<?php echo INCLUDE_PATH_JS.'/metisMenu.min.js';?>"></script>
<script src="<?php echo INCLUDE_PATH_JS.'/custom.js';?>"></script>
<link href="<?php echo INCLUDE_PATH_CSS.'/custom.css';?>" rel="stylesheet">
<!--//Metis Menu -->
<!-- search box jquery -->

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.sell-head{
  padding: 5px 0em;
    background-color: #f5f5f5;
    border-bottom: 1px solid #ddd;
}
#form {
    /*background-color: #FFF;*/
    /*height: 600px;*/
    width: 600px;
    /*margin-right: auto;*/
    /*margin-left: 10em;*/
    /*margin-top: 0px;*/
    /*border-top-left-radius: 10px;*/
    /*border-top-right-radius: 10px;*/
    padding: 0px;
    /*text-align:center;*/
    float: right;

}
#cal{
    font-family: Georgia, "Times New Roman", Times, serif;
    font-size: 14px;
    color: #333;
    height: 20px;
    width: 200px;
    margin-top: 10px;
    margin-left: 10px;
    text-align: right;
    margin-right:15px;
    float:left;
}
 a.btn {
     -webkit-transform: scale(0.8);
     -moz-transform: scale(0.8);
     -o-transform: scale(0.8);
     -webkit-transition-duration: 0.5s;
     -moz-transition-duration: 0.5s;
     -o-transition-duration: 0.5s;
 }
 .file {
  position: relative;
  overflow: hidden;
}
.upload {
  position: absolute;
  font-size: 50px;
  opacity: 0;
  right: 0;
  top: 0;
}
</style>
<!--pie-chart --><!-- index page sales reviews visitors pie chart -->
<script src="<?php echo INCLUDE_PATH_JS.'/pie-chart.js';?>" type="text/javascript"></script>
 <script type="text/javascript">
    </script>
<!-- //pie-chart --><!-- index page sales reviews visitors pie chart -->

  <!-- requried-jsfiles-for owl -->
          <link href="<?php echo INCLUDE_PATH_CSS.'/owl.carousel.css';?>" rel="stylesheet">
          <script src="<?php echo INCLUDE_PATH_JS.'/owl.carousel.js';?>"></script>
            <script type="text/javascript" src="<?php echo INCLUDE_PATH_JS.'/swal.min.js';?>"></script>
          <!-- //requried-jsfiles-for owl -->
</head> 
<body class="cbp-spmenu-push">
  <div class="main-content">
   <?php $this->load->view('includes/side_navbar'); ?>
    <!--left-fixed -navigation-->
    
    <!-- header-starts -->
   
    <!-- //header-ends -->
    <!-- main content start-->
    <div id="page-wrapper">
      <div class="main-page">
        <div class="inline-form widget-shadow">
            <div class="form-title">
              <h4 style="text-align: center;font-weight: bold;">Stock Management</h4>
            </div>
            <form method="POST" action="<?php echo APP_PATH.'StockManagement/'.$action;?>" id="saveproductdetails">
              <input type="hidden" name="productid" value="<?php echo isset($stock[0]['i_uid']) ? $stock[0]['i_uid'] : '' ; ?>">
            <div class="sell-head">              
              <div class="row">
                <div class="col-md-2">
                  <label>Product Code</label>
                  <input type="text" name="product_code" value="" class="form-control">
                </div>
                <div class="col-sm-2">
                  <label>Product Name</label>
                  <input type="text" id="productname" name="productname" value="<?php echo isset($stock[0]['i_product_name']) ? $stock[0]['i_product_name'] : '' ;?>" class="form-control">
                </div>
                <div class="col-sm-2">
                  <label>Product Category</label>
                  <input type="text" name="category" value="<?php echo isset($stock[0]['i_product_category']) ? $stock[0]['i_product_category'] : '' ;?>" required class="form-control"> 
                </div>
                <div class="col-sm-2">
                  <label>Price</label>
                  <input type="number" name="rate" value="<?php echo isset($stock[0]['i_product_rate']) ? $stock[0]['i_product_rate'] : '' ;?>" required class="form-control">
                </div>
                <div class="col-sm-2">
                  <label>Quantity</label>
                  <input type="number" name="quantity" value="<?php echo isset($stock[0]['i_product_count']) ? $stock[0]['i_product_count'] : '' ;?>" required class="form-control">
                </div>

                <div class="col-md-2">
                  <label>QUALITY</label>
                  <select class="form-control">
                    <option selected disabled>Select Quality</option>
                    <option value="O">Original</option>
                    <option value="D">Duplicate</option>
                    <option value="IMP">Imported</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-2">
                  <label>Rack</label>
                  <input type="number" name="rack" value="<?php echo isset($stock[0]['i_product_rack']) ? $stock[0]['i_product_rack'] : '' ;?>" required class="form-control">
                </div>
                <div class="col-sm-4">
                  <label>Description</label>
                    <textarea  rows = "1" id="comment" name="description" required class="form-control"><?php echo isset($stock[0]['i_product_description']) ? $stock[0]['i_product_description'] : '' ;?></textarea>
                  <!--<input type="number" name="rack" style="line-height: 15px!important;">-->
                </div>
                
                
                <!-- <div class="col-sm-4">
                  <label>Amount</label>
                  <input type="number" name="amount" value="<?php //echo isset($stock[0]['i_product_total']) ? $stock[0]['i_product_total'] : '' ;?>" required class="form-control">
                </div> -->
              </div>
              <div class="row">
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-4">
                      <button  class="btn btn-default" id ="submit"style="width: 100%;" onclick="submit();">Save</button>
                    </div>
                    <div class="col-sm-4">
                    </div>
                  </div>
            </div>
             </form >

             <?php
              if($action == 'Addstock'){
              ?>
            <div class="form-body">
             
              <div class="row">
                <form method="post" action="<?php echo APP_PATH.'StockManagement/uploadCsv';?>" enctype="multipart/form-data" id="uploadForm">
                   <div class="col-sm-6">
                    <div class="file btn btn-lg btn-primary file">
                        Upload CSV
                      <input type="file" name="file" class="upload" id="fileselect" onchange="uploadCsv();" accept=".csv"/>
                    </div>
                  </div>
                </form>
              </div>

              <div data-example-id="simple-form-inline" style="overflow-x:scroll!important; height: 280px;"> 
                <!-- <form>  -->
                  <table class="table table-bordered" >
                    <thead>
                      <tr>
                        <!-- <th>Sr No.</th> -->
                        <th>Sr.No</th>
                        <th>Product Name</th>
                        <th>Description</th>                        
                        <th>Quantity</th>
                        <th>Rate</th>
                        <th>Rack</th>
                        <th>Barcode</th>
                        <!-- <th>Amount</th> -->
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i=1;
                      foreach ($stockdata as $values) {

                      ?>
                     <tr>
                       <!-- <td>1</td> -->
                       <td><?php echo $i;?></td>
                       <td><?php echo $values['i_product_name'];?></td>
                       <td><?php echo $values['i_product_description'];?></td>
                       <td><?php echo $values['i_product_count'];?></td>
                       <td><?php echo $values['i_product_rate'];?></td>
                       <td><?php echo $values['i_product_rack'];?></td>
                       <td><img src="<?php echo APP_PATH.'assets/barcode/'.$values['i_product_barcode'];?>"></td>
                       <!-- <td><?php //echo $values['i_product_total'];?></td> -->
                       <td>
                          <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#barcode_<?php echo $values['i_barcodeid'];?>">Print Barcode</a>
                           <a href="<?php echo APP_PATH.'StockManagement/update/'.$values['i_uid'];?>" class="btn btn-primary a-btn-slide-text">
                              <span class="fa fa-pencil-square-o" aria-hidden="true" ></span>
                              <span>Edit</span>            
                          </a>
                          
                          <a href="" class="btn btn-danger a-btn-slide-text" onclick="deleterow(this);">
                          <span>Remove</span> 
                          </a>
                        </td>
                     </tr>
                     <?php 
                     $i++;   
                        }
                      ?>
                    </tbody>
                  </table>
                  </div>
                </div>
              <?php } ?>
                
               
                 <div class="clearfix"></div>
              </div>
            </div>
          </div>        
      </div>
    </div>
  <!--footer-->
  <?php $this->load->view('includes/footer'); ?>
    <!--//footer-->
  </div>
  <?php
  foreach ($stockdata as $values) {
  ?>
  <div class="modal fade" id="barcode_<?php echo $values['i_barcodeid'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Print Barcode</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row" style="margin-top: 0px;">
            <div class="col-md-3"></div>
            <div class="col-md-6">
              <div class="card">
                <div class="card-body" style="border : 1px solid black;padding: 5px;">
                  <span style="float: left;font-weight: 900;"><?php echo $company;?></span>
                  <img src="<?php echo APP_PATH.'assets/barcode/'.$values['i_product_barcode'];?>" style="width: 100%;"><br>
                  <span style="font-size: 13px;font-weight: 900;"><?php echo $values['i_product_name'];?></span>
                  <span style="float: right;font-weight: 900;"><i class="fa fa-inr"></i><?php echo $values['i_product_rate'];?></span>

                </div>
              </div>
            </div>
            <div class="col-md-3"></div>
          </div>
          <div class="row" style="margin-top: 0px;">
            <div class="col-md-3"></div>
            <div class="col-md-6">
              <label>No of Prints : </label>
              <input type="number" name="total_prints" id="total_prints_<?php echo $values['i_product_barcode'];?>" class="form-control">
            </div>
            <div class="col-md-3"></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="print_barcode(this);" data-id = "<?php echo $values['i_product_barcode'];?>">Print</button>
        </div>
      </div>
    </div>
  </div>
  <?php
  }
  ?> 
  <!-- new added graphs chart js-->
  
    <!-- <script src="<?php //echo INCLUDE_PATH_JS.'/Chart.bundle.js';?>"></script> -->
    <!-- <script src="js/utils.js"></script> -->
  
  
  <!-- new added graphs chart js-->
  
  <!-- Classie --><!-- for toggle left push menu script -->
    <script>
      $(document).ready(function() {
        <?php if($this->session->flashdata('response')){ ?>
          swal({
            title: "<?php echo $this->session->flashdata('response'); ?>",
            icon: "success",
          });
        <?php } ?>
    });
       function submit(){
    $("#saveproductdetails").submit();
   }
    // delete row
    function deleterow(btn) {
      var row = btn.parentNode.parentNode;
      row.parentNode.removeChild(row);
    }
    // validation
    $(document).ready(function() {
      $("#submit").click(function() {
        var productname = $("#productname").val();
        var email = $("#email").val();
        if (productname == '' ) {
          alert("Please Fill Required Fields");
        } else {
          alert("Sucess");
        }
      });
    });

    function uploadCsv(){
      $("#uploadForm").submit();
    }

    function print_barcode(el){

    }
    </script>
  <!-- //Classie --><!-- //for toggle left push menu script -->
    
  <!--scrolling js-->
  <script src="<?php echo INCLUDE_PATH_JS.'/jquery.nicescroll.js';?>"></script>
  <script src="<?php echo INCLUDE_PATH_JS.'/scripts.js';?>"></script>
  <!--//scrolling js-->
  
  <!-- side nav js -->
  <script src="<?php echo INCLUDE_PATH_JS.'/SidebarNav.min.js';?>" type='text/javascript'></script>
  <script>
      $('.sidebar-menu').SidebarNav()
    </script>
 
  
  
  <!-- Bootstrap Core JavaScript -->
   <script src="<?php echo INCLUDE_PATH_JS.'/bootstrap.js';?>"> </script>
  <!-- //Bootstrap Core JavaScript -->
  
</body>
</html>