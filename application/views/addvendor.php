<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/bootstrap.css';?>" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/style.css';?>" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/font-awesome.css';?>" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href="<?php echo INCLUDE_PATH_CSS.'/SidebarNav.min.css';?>" media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="<?php echo INCLUDE_PATH_JS.'/jquery-1.11.1.min.js';?>"></script>
<!-- <script src="<?php echo INCLUDE_PATH_JS.'/modernizr.custom.js';?>"></script> -->

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<!-- <script src="<?php echo INCLUDE_PATH_JS.'/Chart.js';?>"></script> -->
<!-- //chart -->

<!-- Metis Menu -->
<script src="<?php echo INCLUDE_PATH_JS.'/metisMenu.min.js';?>"></script>
<script src="<?php echo INCLUDE_PATH_JS.'/custom.js';?>"></script>
<link href="<?php echo INCLUDE_PATH_CSS.'/custom.css';?>" rel="stylesheet">
<script type="text/javascript" src="<?php echo INCLUDE_PATH_JS.'/swal.min.js';?>"></script>
<!--//Metis Menu -->
<!-- search box jquery -->

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.sell-head{
  padding: 5px 0em;
    background-color: #f5f5f5;
    border-bottom: 1px solid #ddd;
}
#form {
    /*background-color: #FFF;*/
    /*height: 600px;*/
    width: 600px;
    /*margin-right: auto;*/
    /*margin-left: 10em;*/
    /*margin-top: 0px;*/
    /*border-top-left-radius: 10px;*/
    /*border-top-right-radius: 10px;*/
    padding: 0px;
    /*text-align:center;*/
    float: right;

}
#cal{
    font-family: Georgia, "Times New Roman", Times, serif;
    font-size: 14px;
    color: #333;
    height: 20px;
    width: 200px;
    margin-top: 10px;
    margin-left: 10px;
    text-align: right;
    margin-right:15px;
    float:left;
}
 a.btn {
     -webkit-transform: scale(0.8);
     -moz-transform: scale(0.8);
     -o-transform: scale(0.8);
     -webkit-transition-duration: 0.5s;
     -moz-transition-duration: 0.5s;
     -o-transition-duration: 0.5s;
 }
</style>
<!--pie-chart --><!-- index page sales reviews visitors pie chart -->
<script src="<?php echo INCLUDE_PATH_JS.'/pie-chart.js';?>" type="text/javascript"></script>
 

</head> 
<body class="cbp-spmenu-push">
  <div class="main-content">
   <?php $this->load->view('includes/side_navbar'); ?>
    <!--left-fixed -navigation-->
    
    <!-- header-starts -->
   
    <!-- //header-ends -->
    <!-- main content start-->
    <div id="page-wrapper">
      <div class="main-page">
        <div class="inline-form widget-shadow">
            <div class="form-title">
              <h4 style="text-align: center;font-weight: bold;">Vendor Details</h4>
            </div>
            <form method="POST" action="<?php echo APP_PATH.'AddVendor/'.$action;?>" id="savevendordetails">
              <input type="hidden" name="vendorid" value="<?php echo isset($vendor[0]['vendor_uid']) ? $vendor[0]['vendor_uid'] : '' ; ?>">
            <div class="sell-head">
              <div class="row">
                <div class="col-sm-4">
                <label style="padding-left: 15px;">Vendor Name:</label>
                <input type="text" name="vendorname" value="<?php echo isset($vendor[0]['vendor_name']) ? $vendor[0]['vendor_name'] : '' ;?>" required>
              </div>
              <div class="col-sm-4">
                 <label>Phone1:&nbsp;</label><input type="text" name="phone1" value="<?php echo isset($vendor[0]['vendor_phone1']) ? $vendor[0]['vendor_phone1'] : '' ?>" required>
              </div>
              <div class="col-sm-4">
                  <label>Phone2:&nbsp;</label><input type="text" name="phone2" value="<?php echo isset($vendor[0]['vendor_phone2']) ? $vendor[0]['vendor_phone2'] : '' ?>" required>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4">
                <label>Contact Person : &nbsp;</label><input type="text" name="contactperson" value="<?php echo isset($vendor[0]['vendor_contact_person']) ? $vendor[0]['vendor_contact_person'] : '' ?>" required>
                  
              </div>
              <div class="col-sm-4">
                <label>Email-Id:&nbsp;</label><input type="email" name="emailid" value="<?php echo isset($vendor[0]['vendor_email_id']) ? $vendor[0]['vendor_email_id'] : '' ?>" required>
              </div>
              <div class="col-sm-4">
                  <label>GST No :&nbsp;</label><input type="number" name="gstno" value="<?php echo isset($vendor[0]['vendor_gstno']) ? $vendor[0]['vendor_gstno'] : '' ?>">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                  <label>Address :</label> <textarea class="" id="" rows="2" name="address" style="width: 230px;margin-left: 50px;" required><?php echo isset($vendor[0]['vendor_address']) ? $vendor[0]['vendor_address'] : '' ?></textarea>
              </div>
            </div>
          
            </div>
            <br/>
            <div class="row">
              <div class="col-sm-12" >
                <div class="col-sm-4">
                  <!-- <button type="submit" class="btn btn-default" style="width: 100px;">Print</button> -->
                </div>
                <div class="col-sm-4">
                  <button  class="btn btn-default" style="width: 100px;" onclick="submit();">Save</button>
                </div>
                <div class="col-sm-4">
                  <button type="submit" class="btn btn-default" style="width: 100px;" onclick="location.reload()">Cancel</button>
                </div>
              </div>
            </div><br/>
            </form>
          </div>        
      </div>
    </div>
  <!--footer-->
  <?php $this->load->view('includes/footer'); ?>
    <!--//footer-->
  </div>
    
  <!-- new added graphs chart js-->
  
    <!-- <script src="<?php echo INCLUDE_PATH_JS.'/Chart.bundle.js';?>"></script> -->
    <!-- <script src="js/utils.js"></script> -->
  
  
  <!-- new added graphs chart js-->
  
  <!-- Classie --><!-- for toggle left push menu script -->
    <script src="js/classie.js"></script>
    <script> 
     $(document).ready(function() {
        <?php if($this->session->flashdata('response')){ ?>
          swal({
            title: "<?php echo $this->session->flashdata('response'); ?>",
            icon: "success",
          });
        <?php } ?>
    });    
       function submit(){
    $("#savevendordetails").submit();
   }

   
    </script>
  <!-- //Classie --><!-- //for toggle left push menu script -->
    
  <!--scrolling js-->
  <script src="<?php echo INCLUDE_PATH_JS.'/jquery.nicescroll.js';?>"></script>
  <script src="<?php echo INCLUDE_PATH_JS.'/scripts.js';?>"></script>
  <!--//scrolling js-->
  
  <!-- side nav js -->
  <script src="<?php echo INCLUDE_PATH_JS.'/SidebarNav.min.js';?>" type='text/javascript'></script>
  <script>
      $('.sidebar-menu').SidebarNav()
    </script>
 
  
  
  <!-- Bootstrap Core JavaScript -->
   <script src="<?php echo INCLUDE_PATH_JS.'/bootstrap.js';?>"> </script>
  <!-- //Bootstrap Core JavaScript -->
  
</body>
</html>