<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/bootstrap.css';?>" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/style.css';?>" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/font-awesome.css';?>" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href="<?php echo INCLUDE_PATH_CSS.'/SidebarNav.min.css';?>" media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="<?php echo INCLUDE_PATH_JS.'/jquery-1.11.1.min.js';?>"></script>
<!-- <script src="<?php echo INCLUDE_PATH_JS.'/modernizr.custom.js';?>"></script> -->

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<!-- <script src="<?php echo INCLUDE_PATH_JS.'/Chart.js';?>"></script> -->
<!-- //chart -->

<!-- Metis Menu -->
<script src="<?php echo INCLUDE_PATH_JS.'/metisMenu.min.js';?>"></script>
<script src="<?php echo INCLUDE_PATH_JS.'/custom.js';?>"></script>
<link href="<?php echo INCLUDE_PATH_CSS.'/custom.css';?>" rel="stylesheet">
<!--//Metis Menu -->
<!-- search box jquery -->

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- datatable css -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.6/css/fixedHeader.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">

<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.sell-head{
  padding: 5px 0em;
    background-color: #f5f5f5;
    border-bottom: 1px solid #ddd;
}
#form {
    /*background-color: #FFF;*/
    /*height: 600px;*/
    width: 600px;
    /*margin-right: auto;*/
    /*margin-left: 10em;*/
    /*margin-top: 0px;*/
    /*border-top-left-radius: 10px;*/
    /*border-top-right-radius: 10px;*/
    padding: 0px;
    /*text-align:center;*/
    /*float: right;*/

}
#cal{
    font-family: Georgia, "Times New Roman", Times, serif;
    font-size: 14px;
    color: #333;
    height: 20px;
    width: 200px;
    margin-top: 10px;
    margin-left: 10px;
    text-align: right;
    margin-right:15px;
    float:left;
}
 a.btn {
     -webkit-transform: scale(0.8);
     -moz-transform: scale(0.8);
     -o-transform: scale(0.8);
     -webkit-transition-duration: 0.5s;
     -moz-transition-duration: 0.5s;
     -o-transition-duration: 0.5s;
 }
</style>
<!--pie-chart --><!-- index page sales reviews visitors pie chart -->
<script src="<?php echo INCLUDE_PATH_JS.'/pie-chart.js';?>" type="text/javascript"></script>
 <script type="text/javascript">

        $(document).ready(function () {
            $('#demo-pie-1').pieChart({
                barColor: '#2dde98',
                trackColor: '#eee',
                lineCap: 'round',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-2').pieChart({
                barColor: '#8e43e7',
                trackColor: '#eee',
                lineCap: 'butt',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-3').pieChart({
                barColor: '#ffc168',
                trackColor: '#eee',
                lineCap: 'square',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

           
        });
         $( function() {
    var availableTags = [
      "ActionScript",
      "AppleScript",
      "Asp",
      "BASIC",
      "C",
      "C++",
      "Clojure",
      "COBOL",
      "ColdFusion",
      "Erlang",
      "Fortran",
      "Groovy",
      "Haskell",
      "Java",
      "JavaScript",
      "Lisp",
      "Perl",
      "PHP",
      "Python",
      "Ruby",
      "Scala",
      "Scheme"
    ];
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  } );

    </script>
<!-- //pie-chart --><!-- index page sales reviews visitors pie chart -->

  <!-- requried-jsfiles-for owl -->
          <link href="<?php echo INCLUDE_PATH_CSS.'/owl.carousel.css';?>" rel="stylesheet">
          <script src="<?php echo INCLUDE_PATH_JS.'/owl.carousel.js';?>"></script>
            <script>
              $(document).ready(function() {
                $("#owl-demo").owlCarousel({
                  items : 3,
                  lazyLoad : true,
                  autoPlay : true,
                  pagination : true,
                  nav:true,
                });
              });
            </script>
          <!-- //requried-jsfiles-for owl -->
</head> 
<body class="cbp-spmenu-push">
  <div class="main-content">
   <?php $this->load->view('includes/side_navbar'); ?>
    <!--left-fixed -navigation-->
    
    <!-- header-starts -->
   
    <!-- //header-ends -->
    <!-- main content start-->

    <div id="page-wrapper">
      <div class="main-page">
        <div class="inline-form widget-shadow " style="margin-top: 0em!important;margin-bottom: 0em!important;">
            

            <br/>
            <div class="form-body">
              <div data-example-id="simple-form-inline" style="overflow-x:scroll!important; "> 
                <!-- <form>  -->
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Invoice Numbers</th>
                            <th>Name</th>
                            <th>Number</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($collection as $key => $value) { ?>
                        <tr>
                          <td><?php echo $value['s_invoice'];?></td>
                          <td><?php echo $value['s_customer_name'];?></td>
                          <td><?php echo $value['s_phone'];?></td>
                          <td><?php echo $value['s_remainig_payment'];?></td>
                          <td><?php echo $value['s_created_at'];?></td>
                          <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#payment_mode_<?php echo $value['s_uid'];?>">Paid</button></td>
                        </tr>
                      <?php } ?>
                    </tbody>
                    
                </table>
                  </div>
                </div>
                </div>
                 <div class="clearfix"></div>
              </div>
            </div>
          </div>        
      </div>
    </div>

    <?php foreach ($collection as $key => $value) { ?>
      <!-- paid modal -->
      <div class="modal fade" id="payment_mode_<?php echo $value['s_uid'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Payment Mode</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <td>
                      <div class="form-check">
                        <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="payment_mode" value="cheque" id="cheque_<?php echo $value['s_uid'];?>"  onclick="ShowHideDiv(this)" data-type="Cheque" data-id="<?php echo $value['s_uid'];?>">&nbsp;Cheque
                      </label>
                      </div>
                    </td>
                    <td>
                      <div class="form-check" style="margin-left: 1em;">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" value="cash" name="payment_mode" id="cash_<?php echo $value['s_uid'];?>" onclick="ShowHideDiv(this)" data-type="cash" data-id="<?php echo $value['s_uid'];?>">&nbsp;Cash
                        </label>
                      </div>
                    </td>
                    <td>
                      <div class="form-check" style="margin-left: 1em;">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" value="card" name="payment_mode" id="card_<?php echo $value['s_uid'];?>" onclick="ShowHideDiv(this)" data-type="Card" data-id="<?php echo $value['s_uid'];?>">&nbsp;Card
                        </label>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              <div id="amountData_<?php echo $value['s_uid'];?>" style="display: none">
                <div class="row">
                  <div class="col-md-4">
                    <label>Amount</label>
                    <input type="number" name="amount" id="namount_<?php echo $value['s_uid'];?>" class="form-control">
                  </div>
                </div>
              </div>
              <div id="chequeData_<?php echo $value['s_uid'];?>" style="display: none">
                <div class="row">
                  <div class="col-md-4">
                    <label>Amount</label>
                    <input type="number" name="amount" id="camount_<?php echo $value['s_uid'];?>" class="form-control">
                  </div>
                  <div class="col-md-4">
                    <label>Bank Name</label>
                    <input type="text" name="bank_name" id="bank_name_<?php echo $value['s_uid'];?>" class="form-control">
                  </div>
                  <div class="col-md-4">
                    <label>Cheque Number</label>
                    <input type="number" name="cheque_number" id="cheque_number_<?php echo $value['s_uid'];?>" class="form-control">
                  </div>
                  <div class="col-md-4">
                    <label>Cheque Date</label>
                    <input type="date" name="cheque_date" id="cheque_date_<?php echo $value['s_uid'];?>" class="form-control">
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="save_collection('<?php echo $value['s_uid'];?>')">Save</button>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
    <!-- paid modal -->
  <!--footer-->
  
    <!--//footer-->
  </div>
    
  <!-- new added graphs chart js-->
  
    <!-- <script src="<?php //echo INCLUDE_PATH_JS.'/Chart.bundle.js';?>"></script> -->
    <!-- <script src="js/utils.js"></script> -->
  
  <script>
    function ShowHideDiv(el) {
      var type = $(el).attr("data-type");
      var id = $(el).attr("data-id");
      if (type=='Cheque'){
        $("#chequeData_"+id).show();
        $("#amountData_"+id).hide();
      }else{
         $("#chequeData_"+id).hide();
         $("#amountData_"+id).show();
      }
    }
       $(document).ready(function() {
    var table = $('#example').DataTable( {
        responsive: true
    } );
 
    new $.fn.dataTable.FixedHeader( table );
} );
    </script>
  <!-- new added graphs chart js-->
  
  <!-- Classie --><!-- for toggle left push menu script -->
    <script src="js/classie.js"></script>
    <script>
      var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        showLeftPush = document.getElementById( 'showLeftPush' ),
        body = document.body;
        
      showLeftPush.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( body, 'cbp-spmenu-push-toright' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );
        disableOther( 'showLeftPush' );
      };
      

      function disableOther( button ) {
        if( button !== 'showLeftPush' ) {
          classie.toggle( showLeftPush, 'disabled' );
        }
      }

      function save_collection(id){
        var mode = $('input[name="payment_mode"]:checked').val();
        if(mode == "cheque"){
          var bank_name = $("#bank_name_"+id).val();
          var cheque_number = $("#cheque_number_"+id).val();
          var amount = $("#camount_"+id).val();
          var cheque_date = $("#cheque_date_"+id).val();
          $.ajax({
            url: "<?php echo APP_PATH.'Home/add_collection';?>", 
            data: {mode : mode, sell_id : id, amount : amount,bank_name : bank_name,cheque_number : cheque_number,cheque_date : cheque_date},
            method: "POST",
            dataType: "json",
            success: function(result){
              console.log(result);
              location.reload();
            }
          });
        }else{
          var amount = $("#namount_"+id).val();
          $.ajax({
            url: "<?php echo APP_PATH.'Home/add_collection';?>", 
            data: {mode : mode, sell_id : id, amount : amount},
            method: "POST",
            dataType: "json",
            success: function(result){
              console.log(result);
              location.reload();
            }
          });
        }
      }
    </script>
  <!-- //Classie --><!-- //for toggle left push menu script -->
    
  <!--scrolling js-->
  // <script src="<?php echo INCLUDE_PATH_JS.'/jquery.nicescroll.js';?>"></script>
  <script src="<?php echo INCLUDE_PATH_JS.'/scripts.js';?>"></script>
  <!--//scrolling js-->
  
  <!-- side nav js -->
  <script src="<?php echo INCLUDE_PATH_JS.'/SidebarNav.min.js';?>" type='text/javascript'></script>
  <script>
      $('.sidebar-menu').SidebarNav()
    </script>
  <!-- //side nav js -->
  
  <!-- for index page weekly sales java script -->
  <script src="<?php echo INCLUDE_PATH_JS.'/SimpleChart.js';?>"></script>
   
  
  <!-- //for index page weekly sales java script -->
  
  
  <!-- Bootstrap Core JavaScript -->
   <script src="<?php echo INCLUDE_PATH_JS.'/bootstrap.js';?>"> </script>
  <!-- //Bootstrap Core JavaScript -->
  <!-- datatbale js -->
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
  <!-- datatbale js -->

</body>
</html>