<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/bootstrap.css';?>" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/style.css';?>" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/font-awesome.css';?>" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href="<?php echo INCLUDE_PATH_CSS.'/SidebarNav.min.css';?>" media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="<?php echo INCLUDE_PATH_JS.'/jquery-1.11.1.min.js';?>"></script>
<!-- <script src="<?php echo INCLUDE_PATH_JS.'/modernizr.custom.js';?>"></script> -->

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<!-- <script src="<?php echo INCLUDE_PATH_JS.'/Chart.js';?>"></script> -->
<!-- //chart -->

<!-- Metis Menu -->
<script src="<?php echo INCLUDE_PATH_JS.'/metisMenu.min.js';?>"></script>
<script src="<?php echo INCLUDE_PATH_JS.'/custom.js';?>"></script>
<link href="<?php echo INCLUDE_PATH_CSS.'/custom.css';?>" rel="stylesheet">
<!--//Metis Menu -->
<!-- search box jquery -->

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo INCLUDE_PATH_JS.'/swal.min.js';?>"></script>
<style>
  #chartdiv {
    width: 100%;
    height: 295px;
  }
  .sell-head{
  padding: 5px 0em;
    background-color: #f5f5f5;
    border-bottom: 1px solid #ddd;
  }
  #form {
    /*background-color: #FFF;*/
    /*height: 600px;*/
    width: 600px;
    /*margin-right: auto;*/
    /*margin-left: 10em;*/
    /*margin-top: 0px;*/
    /*border-top-left-radius: 10px;*/
    /*border-top-right-radius: 10px;*/
    padding: 0px;
    /*text-align:center;*/
    float: right;

  }
  #cal{
    font-family: Georgia, "Times New Roman", Times, serif;
    font-size: 14px;
    color: #333;
    height: 20px;
    width: 200px;
    margin-top: 10px;
    margin-left: 10px;
    text-align: right;
    margin-right:15px;
    float:left;
  }
  a.btn {
     -webkit-transform: scale(0.8);
     -moz-transform: scale(0.8);
     -o-transform: scale(0.8);
     -webkit-transition-duration: 0.5s;
     -moz-transition-duration: 0.5s;
     -o-transition-duration: 0.5s;
  }
</style>
</head> 
<body class="cbp-spmenu-push">
  <div class="main-content">
   <?php $this->load->view('includes/side_navbar'); ?>
    <!--left-fixed -navigation-->
    
    <!-- header-starts -->
   
    <!-- //header-ends -->
    <!-- main content start-->
    <div id="page-wrapper">
      <div class="main-page">
        <div class="inline-form widget-shadow" style="margin-top: 0em!important;margin-bottom: 0em!important;">
            <div class="form-title">
              <h4 style="text-align: center;font-weight: bold;">Purchase Order</h4>
            </div>
            <form action="<?php echo APP_PATH.'Addproduct/save_order';?>" method="POST" id="main_form">
              <input type="hidden" name="order_array" value="" id="order_array">
              <div class="sell-head">
                <div class="row">
                  <div class="col-sm-4">
                    <label style="padding-left: 15px;">Vendor Name:</label>
                    <select style="line-height: 15px!important;" name="vendor_name">
                      <option selected disabled>--Select Vendor name--</option>
                      <?php foreach ($vendors as $key => $value) {?>
                        <option value="<?php echo $value['vendor_uid'];?>"><?php echo $value['vendor_name'];?></option>
                      <?php } ?>
                    </select>
                  </div>
                  
                  <div class="col-sm-4">
                    <label style="padding-left: 15px;">Date Received:</label>
                    <input type="Date" name="date_received" style="line-height: 15px!important;">
                  </div>
                  <div class="col-sm-4">
                    <label style="padding-left: 15px;">PO NO:</label>
                    <input type="Text" name="po_no" style="line-height: 15px!important;" value="<?php echo $invoice;?>" readonly >
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <label style="padding-left: 15px;">Current Date:</label>
                    <input type="Date" name="current_date" id="current_date" style="line-height: 15px!important;">
                  </div>
                  <div class="col-sm-4">
                    <label style="padding-left: 15px;">Payment Type:</label>
                    <select class="" id="sel1" style="line-height: 15px!important; width: 175px;" name="payment_type">
                      <option selected="" disabled="">Please select payment type</option>
                      <option value="cash">Cash</option>
                      <option value="cheque">Cheque</option>
                      <option value="card">Card</option>
                      <option value="credit">On Credit</option>
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <label style="padding-left: 15px;">Created By</label>
                    <input type="text" name="created_by" style="line-height: 15px!important;" value="<?php echo $session_var;?>">
                  </div>
                </div>
                <div class="row" id="row" >
                  <div class="col-sm-12">
                    <div class="ui-widget">
                      <label for="tags">Search by Name/Category: </label>
                      <input id="tags" style="width: 50%;">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-body">
                <div class="row">
                  <div class="col-sm-6">
                    <label>Purchase Details:-</label>
                  </div>
                   <div class="col-sm-6">
                    <a class="btn btn-primary" style="width: 150px;">Print All Barcode</a>
                  </div>
                </div>
                <div data-example-id="simple-form-inline" style="overflow-x:scroll!important; height: 280px;"> 
                  <!-- <form>  -->
                    <table class="table table-bordered" >
                      <thead>
                        <tr>
                          <th>Sr No.</th>
                          <th>Item Name</th>
                          <th>Stock Qty</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="main_order">
                       
                      </tbody>
                    </table>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12" >
                      <div class="col-sm-4">
                        <!-- <button type="submit" class="btn btn-default" style="width: 100px;">Print</button> -->
                      </div>
                      <div class="col-sm-4">
                        <button class="btn btn-default" style="width: 100px;" onclick="submitForm();">Save</button>
                      </div>
                      <div class="col-sm-4">
                        <button class="btn btn-default" style="width: 100px;">Close</button>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  </form>   
                </div>
              </div>
            </div>

      </div>
    </div>
  <!--footer-->
  
    <!--//footer-->
  </div>
  
    <script src="js/classie.js"></script>
   
  <!-- //Classie --><!-- //for toggle left push menu script -->
    
  <!--scrolling js-->
  <script src="<?php echo INCLUDE_PATH_JS.'/jquery.nicescroll.js';?>"></script>
  <script src="<?php echo INCLUDE_PATH_JS.'/scripts.js';?>"></script>
  <!--//scrolling js-->
  
  <!-- side nav js -->
  <script src="<?php echo INCLUDE_PATH_JS.'/SidebarNav.min.js';?>" type='text/javascript'></script>
  <script>
      $('.sidebar-menu').SidebarNav()
    </script>
 
  
  
  <!-- Bootstrap Core JavaScript -->
   <script src="<?php echo INCLUDE_PATH_JS.'/bootstrap.js';?>"> </script>
  
  <script type="text/javascript">
    $(document).ready(function() {
        <?php if($this->session->flashdata('response')){ ?>
          swal({
            title: "<?php echo $this->session->flashdata('response'); ?>",
            icon: "success",
          });
        <?php } ?>
    });
    $(document).ready(function(){
      if(localStorage.getItem("po_order") != null){
        localStorage.removeItem("po_order");
      }
      document.getElementById('current_date').valueAsDate = new Date();
    })
    $( function() {
      var availableTags = [<?php echo '"'.implode('","',  $search_items ).'"' ?>];
      $( "#tags" ).autocomplete({
        source: availableTags,
        select: function(event, ui) {
          $.ajax({
            url: "<?php echo APP_PATH.'Addproduct/addNewItem';?>", 
            data: {additem:ui.item.value},
            method: "POST",
            dataType: "json",
            success: function(result){
              console.log(result);
              if(localStorage.getItem("po_order") === null){
                var order = [{"barcode" : result[0].i_barcodeid, "qty" : 1,"init_price" : result[0].i_product_rate}];
                localStorage.setItem("po_order",JSON.stringify(order));
                var table_string = '';
                if(result != null || result != ''){
                  table_string = '<tr id="row_'+result[0].i_barcodeid+'">'+
                     '<td>'+result[0].i_id+'</td>'+
                     '<td>'+result[0].i_product_name+'</td>'+
                     '<td><input type="text" name="quantity" id="qty_'+result[0].i_barcodeid+'" value="1" style="width: 70px; border:none;" onkeyup="changeQuantity(this)" data-product-id = "'+result[0].i_barcodeid+'"></td>';
                     if(result[0].i_product_count == 0){
                         table_string += '<td><span class="badge badge-danger">Out of stock</span></td>';
                     }else if(result[0].i_product_count > 0 && result[0].i_product_count <= 10){
                       table_string += '<td><span class="badge badge-primary">To be purchased</span></td>';
                     }else{
                       table_string += '<td><span class="badge badge-success">In stock</span></td>';
                     } 
                     table_string += '<td>'+
                        '<a href="#" onclick="remove(this)" data-barcode = '+result[0].i_barcodeid+' class="btn btn-primary a-btn-slide-text">'+
                          '<span class="fa fa-times" aria-hidden="true"></span>'+
                          '<span><strong>Remove</strong></span> '+
                        '</a> '+
                        '<a href="#" class="btn btn-primary a-btn-slide-text">'+
                          '<span class="fa fa-pencil-square-o" aria-hidden="true"></span>'+
                          '<span><strong>Print Barcode</strong></span>'+            
                        '</a>'+
                      '</td>'+
                  '</tr>';

                  $("#main_order").append(table_string);
                }
                // calculatetotal();
              }else{
                var order = localStorage.getItem("po_order");
                var jsonOrder = JSON.parse(order);
                if(_isContains(jsonOrder,result[0].i_barcodeid)){
                  $.each(jsonOrder, function(index,value){
                    if(value.barcode == result[0].i_barcodeid){
                      var pre = value.qty;
                      var newq = Number(pre) + 1;
                      value.qty = newq;
                      // var check = checkQuantity(newq,value.barcode);
                      // if(check == true){
                        $("#qty_"+result[0].i_barcodeid).val(newq);
                      // }else{

                      // }
                      // var current_price = value.init_price;
                      // var new_price = newq * Number(current_price);
                      // $("#"+result[0].i_id).text(new_price);
                      

                    }
                  });
                  localStorage.setItem("po_order",JSON.stringify(jsonOrder));
                  // calculatetotal();
                }else{
                  var order = {"barcode" : result[0].i_barcodeid, "qty" : 1,"init_price" : result[0].i_product_rate};
                  jsonOrder.push(order);
                  localStorage.setItem("po_order",JSON.stringify(jsonOrder));
                  if(result != null || result != ''){                                       
                    table_string ='<tr id="row_'+result[0].i_barcodeid+'">'+
                       '<td>'+result[0].i_id+'</td>'+
                       '<td>'+result[0].i_product_name+'</td>'+
                       '<td><input type="text" name="quantity" value="1" id="qty_'+result[0].i_barcodeid+'" style="width: 70px; border:none;" onkeyup="changeQuantity(this)" data-product-id = "'+result[0].i_barcodeid+'"></td>';
                       if(result[0].i_product_count == 0){
                           table_string += '<td><span class="badge badge-danger">Out of stock</span></td>';
                       }else if(result[0].i_product_count > 0 && result[0].i_product_count <= 10){
                         table_string += '<td><span class="badge badge-primary">To be purchased</span></td>';
                       }else{
                         table_string += '<td><span class="badge badge-success">In stock</span></td>';
                       } 
                       table_string += '<td>'+
                          '<a href="#" onclick="remove(this)" data-barcode = '+result[0].i_barcodeid+' class="btn btn-primary a-btn-slide-text">'+
                            '<span class="fa fa-times" aria-hidden="true"></span>'+
                            '<span><strong>Remove</strong></span> '+
                          '</a> '+
                          '<a href="#" class="btn btn-primary a-btn-slide-text">'+
                            '<span class="fa fa-pencil-square-o" aria-hidden="true"></span>'+
                            '<span><strong>Print Barcode</strong></span>'+            
                          '</a>'+
                        '</td>'+
                      '</tr>';
                      $("#main_order").append(table_string);
                      $("#tags").val('');
                  }
                  // calculatetotal();
                }

              }

              $("#tags").val('');
              
            }
          });
        }
      });
    });


    function changeQuantity(el){
      var quantity = $(el).val();
      if(quantity != ''){
        var barcode = $(el).attr("data-product-id");
        var order = localStorage.getItem("po_order");
          var jsonOrder = JSON.parse(order);
          if(_isContains(jsonOrder,barcode)){
            $.each(jsonOrder, function(index,value){
              if(value.barcode == barcode){
                var newq = quantity;
                value.qty = newq;
                $("#qty_"+barcode).val(newq);
              }
            });
            localStorage.setItem("po_order",JSON.stringify(jsonOrder));
          }
        }
      }

    // function checkQuantity(qty,barcode){
    //   $.ajax({
    //     url: "<?php //echo APP_PATH.'Addproduct/checkQuantity';?>", 
    //     data: { qty : qty , barcode : barcode},
    //     method: "POST",
    //     dataType: "json",
    //     success: function(result){
    //       console.log(result);
    //       if(result.status == "yes"){
    //         return true;
    //       }else{
    //         swal({
    //           title: "Item not available!",
    //           icon: "error",
    //         }).then(function(){
    //           $("#qty_"+result.id).val(result.qty);
    //         });
            
    //       }
    //     }
    //   });
    // }

    function submitForm(){
      var finalOrder = localStorage.getItem("po_order");
      $("#order_array").val(finalOrder);
      $("#main_form").submit();
    }

    function remove(el){
      var barcode = $(el).attr('data-barcode');
      $("#row_"+barcode).remove();
      var finalOrder = localStorage.getItem("po_order");
      var jsonOrder = JSON.parse(finalOrder);
      var len = jsonOrder.length;
      var newArr = [];
      if(len > 1){
        $.each(jsonOrder, function(index,value){
          if(value.barcode == barcode){
          }else{
            newArr.push(value);
          }
        });
        localStorage.setItem("po_order",JSON.stringify(newArr));
      }else{
        localStorage.removeItem("po_order");
      }
      
    }

    function removeNulls(obj) {
      var isArray = obj instanceof Array;
      for (var k in obj) {
        if (obj[k] === null) isArray ? obj.splice(k, 1) : delete obj[k];
        else if (typeof obj[k] == "object") removeNulls(obj[k]);
        if (isArray && obj.length == k) removeNulls(obj);
      }
      return obj;
    }


    function _isContains(json, value) {
      let contains = false;
      Object.keys(json).some(key => {
          contains = typeof json[key] === 'object' ? _isContains(json[key], value) : json[key] === value;
           return contains;
      });
       return contains;
    }
  </script>
</body>
</html>