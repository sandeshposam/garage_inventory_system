
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/bootstrap.css';?>" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/style.css';?>" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo INCLUDE_PATH_CSS.'/font-awesome.css';?>" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href="<?php echo INCLUDE_PATH_CSS.'/SidebarNav.min.css';?>" media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="<?php echo INCLUDE_PATH_JS.'/jquery-1.11.1.min.js';?>"></script>
<!-- <script src="<?php echo INCLUDE_PATH_JS.'/modernizr.custom.js';?>"></script> -->

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<!-- <script src="<?php echo INCLUDE_PATH_JS.'/Chart.js';?>"></script> -->
<!-- //chart -->

<!-- Metis Menu -->
<script src="<?php echo INCLUDE_PATH_JS.'/metisMenu.min.js';?>"></script>
<script src="<?php echo INCLUDE_PATH_JS.'/custom.js';?>"></script>
<link href="<?php echo INCLUDE_PATH_CSS.'/custom.css';?>" rel="stylesheet">
<!--//Metis Menu -->
<!-- search box jquery -->

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>

</style>
<!--pie-chart --><!-- index page sales reviews visitors pie chart -->
<script src="<?php echo INCLUDE_PATH_JS.'/swal.min.js';?>" type="text/javascript"></script>
 

</head> 
<body class="cbp-spmenu-push">
  <!-- <div class="main-content"> -->
 
    <!--left-fixed -navigation-->
    
    <!-- header-starts -->
   
    <!-- //header-ends -->
    <!-- main content start-->
   <!-- main content start-->
    <div id="page-wrapper" style="background-color: #ffffff !important;">
      <div class="main-page login-page " style="margin-right: 22em;">
        <h2 class="title1">Login</h2>
        <div class="widget-shadow">
          <div class="login-body">
            <form action="<?php echo APP_PATH.'Login/validate_email';?>" method="post">
              <input type="email" class="user" name="email" placeholder="Enter Your Email" required="">
              <!-- <input type="password" name="password" class="lock" placeholder="Password" required=""> -->
              
              <input type="submit" name="Sign In" value="Sign In">
              <!-- <div class="registration">
                Don't have an account ?
                <a class="" href="signup.html">
                  Create an account
                </a>
              </div> -->
            </form>
          </div>
        </div>
        
      </div>
    </div>
  <!--footer-->
  
    <!--//footer-->
  <!-- </div> -->
    
  <!-- new added graphs chart js-->
  
    <!-- <script src="<?php echo INCLUDE_PATH_JS.'/Chart.bundle.js';?>"></script> -->
    <!-- <script src="js/utils.js"></script> -->
  
  
  <!-- new added graphs chart js-->
  
  <!-- Classie --><!-- for toggle left push menu script -->
    <script src="js/classie.js"></script>
    <script>     
       function submit(){
    $("#savevendordetails").submit();
   }

   
    </script>
  <!-- //Classie --><!-- //for toggle left push menu script -->
    
  <!--scrolling js-->
  <script src="<?php echo INCLUDE_PATH_JS.'/jquery.nicescroll.js';?>"></script>
  <script src="<?php echo INCLUDE_PATH_JS.'/scripts.js';?>"></script>
  <!--//scrolling js-->
  
  <!-- side nav js -->
  <script src="<?php echo INCLUDE_PATH_JS.'/SidebarNav.min.js';?>" type='text/javascript'></script>
  <script>
      $('.sidebar-menu').SidebarNav()
    </script>
 
  
  
  <!-- Bootstrap Core JavaScript -->
   <script src="<?php echo INCLUDE_PATH_JS.'/bootstrap.js';?>"> </script>
  <!-- //Bootstrap Core JavaScript -->
  <script type="text/javascript">
    $(document).ready(function() {
        <?php if($this->session->flashdata('response')){ ?>
          swal({
            title: "<?php echo $this->session->flashdata('response'); ?>",
            icon: "error",
          });
        <?php } ?>
    });
  </script>
</body>
</html>