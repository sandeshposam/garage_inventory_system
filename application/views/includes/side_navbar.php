<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
    <!--left-fixed -navigation-->
    <aside class="sidebar-left">
      <nav class="navbar navbar-inverse">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <h1><a class="navbar-brand" href="index.html"><span class="fa fa-area-chart"></span> Garage<span class="dashboard_text">Inventory System</span></a></h1>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="sidebar-menu">
              <li class="header">MAIN NAVIGATION</li>
              <li class="treeview">
                <a href="#">
                  
                <i class="fa fa-cart-plus"></i>
                <span>Sell</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu " id="nav">
                  <li class="active"><a href="<?php echo APP_PATH.'Home';?>"><i class="fa fa-angle-right"></i> Add Sell</a></li>
                  <li><a href="<?php echo APP_PATH.'Home/Sales';?>"><i class="fa fa-angle-right"></i> Sales</a></li>
                  <li><a href="<?php echo APP_PATH.'Home/Collection';?>"><i class="fa fa-angle-right"></i> Collection</a></li>
                  <!-- <li><a href="media.html"><i class="fa fa-angle-right"></i>Reports</a></li> -->
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                <i class="fa fa-shopping-cart"></i>
                <span>Purchase</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?php echo APP_PATH.'Addproduct';?>"><i class="fa fa-angle-right"></i> Add</a></li>
                  <li><a href="<?php echo APP_PATH.'Addproduct/payout';?>"><i class="fa fa-angle-right"></i> Payouts</a></li>
                  
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                <i class="fa fa-stack-exchange"></i>
                <span>Stock Management</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?php echo APP_PATH.'StockManagement';?>"><i class="fa fa-angle-right"></i> ADD</a></li>
                  <li><a href="<?php echo APP_PATH.'StockManagement/To_purchase';?>"><i class="fa fa-angle-right"></i>To be Purchase</a></li>
                  
                </ul>
              </li>
              <li class="treeview">
                <a href="<?php echo APP_PATH.'AddVendor';?>">
                <i class="fa fa-user-plus"></i>
                <span> Vendor</span>
               <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                   <li><a href="<?php echo APP_PATH.'AddVendor';?>"><i class="fa fa-angle-right"></i>Manage Vendor</a></li>
                  <li><a href="<?php echo APP_PATH.'AddVendor/view';?>"><i class="fa fa-angle-right"></i>View Vendor</a></li>
                  <!-- <li><a href=""><i class="fa fa-angle-right"></i>Update</a></li> -->
                </ul>
                </a>
              </li>
             
             
              <li class="treeview">
                <a href="#">
                <i class="fa fa-calendar-o"></i>
                <span>Audit</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?php echo APP_PATH.'';?>"><i class="fa fa-angle-right"></i> ADD</a></li>
                  <!-- <li><a href="media.html"><i class="fa fa-angle-right"></i>Reports</a></li> -->
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                <i class="fa fa-file"></i>
                <span>Reports</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?php echo APP_PATH.'Reports/sell';?>"><i class="fa fa-angle-right"></i> Sell</a></li>
                  <li><a href="<?php echo APP_PATH.'Reports/purchase';?>"><i class="fa fa-angle-right"></i>Purchase</a></li>
                  <li><a href="<?php echo APP_PATH.'Reports/customer';?>"><i class="fa fa-angle-right"></i>Customer</a></li>
                  <li><a href="<?php echo APP_PATH.'Reports/vendor';?>"><i class="fa fa-angle-right"></i>Vendor</a>
                  </li>
                </ul>
              </li>
               <li class="treeview">
                <a href="<?php echo APP_PATH.'Changegst';?>">
                <i class="fa fa-wrench"></i> <span>Setting</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
               <ul class="treeview-menu">
                  <li><a href="<?php echo APP_PATH.'Changegst';?>"><i class="fa fa-angle-right"></i> GST</a></li>
                  <li><a href=""><i class="fa fa-angle-right"></i>Change Password</a></li>
                  <li><a href="<?php echo APP_PATH.'Profile';?>"><i class="fa fa-angle-right"></i>Profile</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                <i class="fa fa-sign-out"></i> <span>Logout</span>
                <!-- <i class="fa fa-angle-left pull-right"></i> -->
                </a>
                <!-- <ul class="treeview-menu">
                  <li><a href="forms.html"><i class="fa fa-angle-right"></i> General Forms</a></li>
                  <li><a href="validation.html"><i class="fa fa-angle-right"></i> Form Validations</a></li>
                </ul> -->
              </li>
              <!-- <li class="treeview">
                <a href="#">
                <i class="fa fa-table"></i> <span>Tables</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="tables.html"><i class="fa fa-angle-right"></i> Simple tables</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                <i class="fa fa-envelope"></i> <span>Mailbox </span>
                <i class="fa fa-angle-left pull-right"></i><small class="label pull-right label-info1">08</small><span class="label label-primary1 pull-right">02</span></a>
                <ul class="treeview-menu">
                  <li><a href="inbox.html"><i class="fa fa-angle-right"></i> Mail Inbox </a></li>
                  <li><a href="compose.html"><i class="fa fa-angle-right"></i> Compose Mail </a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                <i class="fa fa-folder"></i> <span>Examples</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="login.html"><i class="fa fa-angle-right"></i> Login</a></li>
                  <li><a href="signup.html"><i class="fa fa-angle-right"></i> Register</a></li>
                  <li><a href="404.html"><i class="fa fa-angle-right"></i> 404 Error</a></li>
                  <li><a href="500.html"><i class="fa fa-angle-right"></i> 500 Error</a></li>
                  <li><a href="blank-page.html"><i class="fa fa-angle-right"></i> Blank Page</a></li>
                </ul>
              </li>
              <li class="header">LABELS</li>
              <li><a href="#"><i class="fa fa-angle-right text-red"></i> <span>Important</span></a></li>
              <li><a href="#"><i class="fa fa-angle-right text-yellow"></i> <span>Warning</span></a></li>
              <li><a href="#"><i class="fa fa-angle-right text-aqua"></i> <span>Information</span></a></li>
            </ul> -->
          </div>
          <!-- /.navbar-collapse -->
      </nav>
    </aside>
  </div>
  <script type="text/javascript">


  </script>